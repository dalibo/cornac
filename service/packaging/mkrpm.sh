#!/bin/bash -eux
#
# Usage: mkrpm [dist/pgCornac*.whl]
#
# Wraps cornac bundle in a rpm.
#

# Defaults to last built wheel.
pkgdir=$(readlink -m "$0/..")
top_srcdir=$(readlink -m "$0/../..")
distdir="${top_srcdir}/dist"
dist=${1-$distdir/pgCornac-last.whl}
workdir=$(mktemp --directory -t cornac-rpm-XXX)

# Test for build requirements.
type -p fpm
type -p rpmbuild
type -p yum

teardown () {
	if [ "0" = "${CLEAN-1}" ] ; then
		return
	fi

	rm -rf "$workdir"
	yum remove -q -y cornac

	if ! [ -f "${rpm-}" ] ; then
		return
	fi

	# Now display a nice message on success.
	set +x
	cat <<-EOF


	 	RPM package is available at ${rpm#/workspace/}.


	EOF
}
trap teardown EXIT INT TERM

# If dist is a filename, compute its absolute path.
if [ -z "${dist%%*pgCornac-*.*}" ] ; then
	# Compute root for relative path.
	if [ -n "${COMPOSEBUILD-}" ] ; then
		# Compose build is relative to docker-compose.yml directory.
		callerwd="$(readlink -m "$0/..")"
	fi
	dist=$(cd "${callerwd-.}"; readlink -e "$dist")
	test -f "$dist"
fi

# builddeps
yum -q -y install python36 python36-devel libev-devel libvirt-devel

# BUILD

export DESTDIR=${workdir}/build
"$pkgdir/install.sh" "$dist" "$distdir/requirements.txt"
version=$(cd /; "$DESTDIR/opt/cornac/bin/python" -c 'import pkg_resources as p; print(p.get_distribution("pgCornac").version)')

rpm=$(readlink -m "$distdir/cornac-service-$version-1.x86_64.rpm")
fpm --verbose \
    --force \
    --debug-workspace \
    --workdir "$workdir" \
    --input-type dir \
    --output-type rpm \
    --package "$rpm" \
    --name cornac-service \
    --version "$version" \
    --iteration 1 \
    --architecture amd64 \
    --description "RDS-compatible Managed-Postgres Webservice" \
    --category database \
    --license PostgreSQL \
    --depends libev \
    --depends libvirt-libs \
    --depends virt-install \
    --depends nginx \
    --depends openssl \
    --depends policycoreutils-python \
    --depends python36 \
    --depends sudo \
    --after-install $pkgdir/postinst.sh \
    --after-remove $pkgdir/postrm.sh \
    "$DESTDIR/=/"

# TEST

test -f "$rpm"

yum install -q -y "$rpm"
(cd /; LANG=en_US.utf8 /opt/cornac/bin/cornac --version)

ln -fs "${rpm##*/}" "${rpm%/*}/cornac-service-last.rpm"
chown "$(stat -c %u:%g "$0")" "$rpm" "$_"
