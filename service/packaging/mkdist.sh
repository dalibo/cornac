#!/bin/bash -eux

top_srcdir=$(readlink -m "$0/../..")
cd "$top_srcdir"

# Test for build requirements.
test -f pyproject.toml
type -p poetry

mkdir -p "${XDG_CACHE_DIR-${HOME}/.cache}"

readarray -t files < <(poetry build | tee /dev/stderr | grep -Po 'Built \KpgCornac-.*.whl')

# Now, symlink -last dist so that mkrpm.sh will safely defaults to latest build.
#
# Poetry uses semver in pyproject.toml, but translate to PEP440 version in final
# dist artefacts. Thus, we extract PEP440 version from wheel filename where
# version is surrounded by hyphens.
#
# See. https://github.com/sdispater/poetry/issues/1389
whl=${files[0]}
ownership=$(stat -c %u:%g pyproject.toml)
ln -fs "$whl" "dist/${whl/-*.whl/-last.whl}"
chown "$ownership" "dist/$whl" "$_"
