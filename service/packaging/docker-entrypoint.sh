#!/bin/bash -eux

: "${CORNAC_CANONICAL_URL=http://$(hostname -i):8001}"
export CORNAC_CANONICAL_URL

if [ -n "${CORNAC_NOBOOTSTRAP-}" ] ; then
	# Wait for database to be migrated by another container.
	for s in {4..0} ; do
		if cornac --verbose migratedb --check ; then
			break
		else
			sleep $s
		fi
	done
else
	if ! cornac --verbose migratedb --check ; then
		cornac --verbose bootstrap --master-email "${CORNAC_MASTER_EMAIL-remi.root@acme.tld}"
	fi
fi

exec cornac --verbose "$@"
