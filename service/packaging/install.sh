#!/bin/bash -eux
#
# Install cornac in a dedicated virtualenv /opt/cornac. This script respects
# DESTDIR environment variable.
#

dist=${1-pgCornac}
requirements=${2-requirements.txt}
DESTDIR=$(readlink -m "${DESTDIR-/}")
DESTDIR=${DESTDIR%/}

mkdir -p ~/.cache
chown -R root:root ~/.cache

#       VIRTUALENV INSTALLATION

VIRTUAL_ENV=${DESTDIR}/opt/cornac
mkdir -p "$VIRTUAL_ENV"
python3 -m venv --copies "$VIRTUAL_ENV"
(
	# subshell in virtualenv.
	export PATH=${VIRTUAL_ENV}/bin:$PATH
	hash -r pip3  # Ensure we use pip3 from virtualenv.
	pip3 install --upgrade pip setuptools wheel
	pip3 install --ignore-installed --no-deps \
	     --requirement "$requirements" \
	     psycopg2-binary \
	     libvirt-python \
	     "$dist"
	pip3 check
)
if [ -n "${DESTDIR%/}" ] ; then
	"${0%/*}/relocate.sh" "$VIRTUAL_ENV" "/opt/cornac"
fi

#       SCRIPTS AND UNIT

datadir=$(find "${VIRTUAL_ENV}/" -name "site-packages")/cornac/files
test -d "$datadir"

install --mode 0755 --directory "$DESTDIR/usr/local/lib/systemd/system"
install --mode 0644 "$datadir"/cornac-{scheduler,web,worker@}.service "$DESTDIR/usr/local/lib/systemd/system/"

# Symlink scripts in PATH.
reldatadir=../lib/${datadir##*/lib/}
test "$(readlink -e "${DESTDIR}/opt/cornac/bin/${reldatadir}")" = "$datadir"
ln -fs \
   "$reldatadir/cornac-script" \
   "$reldatadir/cornac-shell" \
   "$reldatadir/cornac-setup" \
   "$DESTDIR/opt/cornac/bin"
