#!/bin/bash -eux

origin="${1%/}"
target="${2%/}"

for bin in "$origin"/bin/* ; do
	if ! [ -f "$bin" ] ; then
		continue
	fi

	if head -1 "$bin" | grep -Fq "$origin" ; then
		sed -i "1s,$origin/bin/python,$target/bin/python," $bin
	fi
done
