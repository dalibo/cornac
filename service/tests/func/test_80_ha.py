import os

import pytest


if 'CORNAC_ETCD' not in os.environ:
    pytest.skip('No etcd.', allow_module_level=True)


PGPASSWORD = 'C0nf\'de tie!'  # The baddest password to send thru SSH.


def test_create_db_instance(aws, cornacc, rds, worker):
    out = aws(
        "rds", "create-db-instance",
        "--db-instance-identifier", "ha",
        "--db-instance-class", "db.t2.micro",
        "--engine", "postgres",
        "--engine-version", "12",
        "--allocated-storage", "5",
        "--master-username", "master",
        "--master-user-password", PGPASSWORD,
        "--multi-az",
    )
    assert 'creating' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance(identifier='ha')

    cornacc.login()
    r = cornacc.get('/cornac/virtual-ip-ranges')
    r.raise_for_status()
    assert 1 == r.json()['ranges'][0]['allocated']


def test_sql_to_endpoint(aws, pgconnect, rds):
    out = aws(
        "rds", "describe-db-instances", "--db-instance-identifier", "ha")
    with pgconnect(out['DBInstances'][0], password=PGPASSWORD) as curs:
        curs.execute("SELECT NOW()")


@pytest.mark.slow
def test_start_and_stop(aws, rds, worker):
    aws("rds", "stop-db-instance", "--db-instance-identifier", "ha")
    aws.wait_instance(identifier="ha", wanted="stopped")
    aws("rds", "start-db-instance", "--db-instance-identifier", "ha")
    aws.wait_instance(identifier="ha")


def test_failover(aws, pgconnect, rds):
    out = aws(
        "rds", "reboot-db-instance", "--db-instance-identifier", "ha",
        "--force-failover",
    )
    assert 'rebooting' == out['DBInstance']['DBInstanceStatus']
    aws.wait_instance(identifier='ha')

    with pgconnect(out['DBInstance'], password=PGPASSWORD) as curs:
        curs.execute("SELECT NOW()")


def test_delete_db_instance(aws, iaas, rds, worker):
    machine_count = len(list(iaas.list_machines()))
    out = aws(
        "rds", "delete-db-instance",
        "--db-instance-identifier", "ha",
        "--skip-final-snapshot",
    )
    assert 'deleting' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance(identifier='ha', wanted='__deleted__')

    new_machine_count = len(list(iaas.list_machines()))

    assert machine_count - 2 == new_machine_count


def test_restore_db_instance(aws, cornacc, cornac_env, rds, pgconnect, worker):
    out = aws(
        "rds", "describe-db-snapshots",
        "--db-instance-identifier", "cornac",
    )
    snapshot_id = out['DBSnapshots'][0]['DBSnapshotIdentifier']
    out = aws(
        "rds", "restore-db-instance-from-db-snapshot",
        "--db-snapshot-identifier", snapshot_id,
        "--db-instance-identifier", "rest-ha",
        "--multi-az",
    )
    assert 'creating' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance(identifier='rest-ha')

    cornacc.login()
    r = cornacc.get('/cornac/virtual-ip-ranges')
    r.raise_for_status()
    assert 1 == r.json()['ranges'][0]['allocated']

    out = aws(
        "rds", "describe-db-instances", "--db-instance-identifier", "rest-ha")
    # This is the password used for cornac bootstrap.
    pgpassword = cornac_env['PGPASSWORD']
    with pgconnect(out['DBInstances'][0], password=pgpassword) as curs:
        curs.execute("SELECT NOW()")

    out = aws(
        "rds", "delete-db-instance",
        "--db-instance-identifier", "rest-ha",
        "--skip-final-snapshot",
    )
    assert 'deleting' == out['DBInstance']['DBInstanceStatus']
    aws.wait_instance(identifier='rest-ha', wanted='__deleted__')
