import pytest
from sh import cornac

from cornac.operator.machine import MachineCork


PGPASSWORD = 'C0nf\'de tie!'  # The baddest password to send thru SSH.


def test_describe_db_instances(aws, rds):
    out = aws("rds", "describe-db-instances")
    assert 'postgres' == out['DBInstances'][0]['Engine']


def test_describe_orderable_db_instance_options(aws, rds):
    out = aws(
        "rds", "describe-orderable-db-instance-options",
        "--engine", "postgres",
    )
    assert 4 < len(out['OrderableDBInstanceOptions'])
    assert 'postgres' == out['OrderableDBInstanceOptions'][0]['Engine']


def test_create_db_instance(aws, cornac_env, rds, worker):
    if 'CORNAC_TEMBOARD' in cornac_env:
        args = ["--enable-performance-insights"]
    else:
        args = []

    out = aws(
        "rds", "create-db-instance",
        "--db-instance-identifier", "test0",
        "--db-instance-class", "db.t2.small",
        "--availability-zone", "local1b",
        "--engine", "postgres",
        "--engine-version", "11",
        "--allocated-storage", "5",
        "--no-multi-az",
        "--master-username", "master",
        "--master-user-password", PGPASSWORD,
        *args
    )
    assert 'creating' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance('available')


def test_sql_to_endpoint(aws, pgconnect, rds):
    out = aws(
        "rds", "describe-db-instances", "--db-instance-identifier", "test0")
    with pgconnect(out['DBInstances'][0], password=PGPASSWORD) as curs:
        curs.execute("SELECT NOW()")


def test_temboard(temboardc, cornacc, iaas):
    r = cornacc.get(
        '/cornac/temboard/redirect/000000000001/test0',
        allow_redirects=False
    )
    assert r.is_redirect

    r = temboardc.get(r.headers['location'])
    r.raise_for_status()


@pytest.mark.slow
def test_reboot_db_instance(aws, pgconnect, rds, worker):
    out = aws(
        "rds", "reboot-db-instance",
        "--db-instance-identifier", "test0",
    )
    assert 'rebooting' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance('available')

    with pgconnect(out['DBInstance'], password=PGPASSWORD) as curs:
        curs.execute("SELECT NOW()")


@pytest.mark.slow
def test_inspect_stopped(aws, cornac_env, iaas, rds):
    machine = MachineCork(identifier='test0', zone='local1b')
    iaas.stop_machine(machine)
    assert not iaas.is_running(machine)
    cornac("--verbose", "inspect", _err_to_out=True, _env=cornac_env)
    aws.wait_instance(identifier='test0', wanted='stopped', first_delay=3)


def test_delete_db_instance(aws, iaas, rds, worker):
    machine_count = len(list(iaas.list_machines()))
    out = aws(
        "rds", "delete-db-instance",
        "--db-instance-identifier", "test0",
        "--skip-final-snapshot",
    )
    assert 'deleting' == out['DBInstance']['DBInstanceStatus']

    aws.wait_instance(identifier='test0', wanted='__deleted__', first_delay=5)

    new_machine_count = len(list(iaas.list_machines()))

    assert machine_count - 1 == new_machine_count
