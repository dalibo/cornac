import json.decoder
import logging
import os
import sys
from contextlib import contextmanager
from functools import partial
from io import StringIO
from pathlib import Path
from subprocess import Popen
import tempfile
from time import sleep
from urllib.parse import urlparse

import httpx
import psycopg2
import pytest
from sh import ErrorReturnCode, aws as awscli, cornac, createdb, dropdb, ssh

if "silent-flake8-E402":
    sys.path[:0] = ['/opt/cornac/lib/python3.6/site-packages']

from cornac import create_app
from cornac.iaas import IaaS
from cornac.temboard import TemBoard
from cornac.etcd import connect as etcd_connect, EtcdKeyNotFound


logger = logging.getLogger(__name__)


@pytest.fixture(scope='session')
def app(cornac_env):
    app = create_app(environ=cornac_env)
    with app.app_context():
        yield app


@pytest.fixture(scope='session')
def db():
    if os.environ['CORNAC_IAAS'].startswith('none+'):
        # Reuse PGHOST as cornac database server. Create a dedicated database
        # for tests.
        pgenv = dict(
            {k: v for k, v in os.environ.items() if k.startswith('PG')},
            PGDATABASE="cornactest",
        )
        try:
            createdb(pgenv['PGDATABASE'], _env=pgenv)
            yield pgenv
        finally:
            dropdb(pgenv['PGDATABASE'], _env=pgenv)
    else:
        yield {}


class AWSCLI(object):
    class FastFail(Exception):
        pass

    class Timeout(Exception):
        pass

    def __init__(self, env):
        self.env = env

    def __call__(self, *a, **kw):
        err = StringIO()
        kw.setdefault('_err', err)
        kw.setdefault('_env', self.env)
        try:
            out = awscli(*a, **kw)
        except ErrorReturnCode as e:
            e.stderr = err.getvalue()
            raise

        try:
            out = json.loads(out.stdout)
        except json.decoder.JSONDecodeError:
            pass
        return out

    def profile(self, name):
        return self.__class__(dict(self.env, AWS_PROFILE=name))

    def with_credentials(self, access_key, secret):
        return self.__class__(dict(
            self.env,
            AWS_ACCESS_KEY_ID=access_key,
            AWS_SECRET_ACCESS_KEY=secret,
        ))

    def wait_account(
            self, wanted='ACTIVE', account_id='000000000001',
            fail='XFAILED'):
        # Wait for account deletion with wanted='__deleted__'.
        for s in range(0, 10):
            out = self(
                "organizations", "describe-account",
                "--account-id", account_id,
            )
            status = out['Account']['Status']
            if wanted == status:
                return out['Account']
            elif fail and fail == status:
                raise self.FastFail("Account failed.")
            sleep(s)
        else:
            raise self.Timeout("Timeout checking for status update.")

    def wait_instance(self, wanted='available', identifier='test0',
                      first_delay=20, fail='failed'):
        # Wait for instance deletion with wanted='__deleted__'.
        for s in range(first_delay, 1, -1):
            out = self(
                "rds", "describe-db-instances",
                "--db-instance-identifier", identifier)
            if '__deleted__' == wanted:
                if not out['DBInstances']:
                    break
            else:
                status = out['DBInstances'][0]['DBInstanceStatus']
                if wanted == status:
                    return out['DBInstances'][0]
                elif fail and fail == status:
                    raise self.FastFail("Instance failed.")
            sleep(s)
        else:
            raise self.Timeout("Timeout checking for status update.")

    def wait_snapshot(self, wanted='available', identifier='test0',
                      first_delay=8):
        for s in range(first_delay, 1, -1):
            out = self(
                "rds", "describe-db-snapshots",
                "--db-snapshot-identifier", identifier)
            if '__deleted__' == wanted:
                if not out['DBSnapshots']:
                    break
            try:
                out['DBSnapshots'][0]
            except IndexError:
                # snapshots disappeared
                if wanted is None:
                    # That what's we're waiting for.
                    break
                raise
            if wanted == out['DBSnapshots'][0]['Status']:
                break
            sleep(s)
        else:
            raise self.Timeout("Timeout checking for status update.")


@pytest.fixture(scope='session')
def aws_env(cornac_env):
    awscli_config = Path(__file__).parent.parent / 'awscli-config'
    yield dict(
        cornac_env,
        AWS_CONFIG_FILE=str(awscli_config / 'config'),
        AWS_PROFILE='master',
        AWS_SHARED_CREDENTIALS_FILE=str(awscli_config / 'credentials')
    )


@pytest.fixture(scope='session')
def aws(aws_env):
    yield AWSCLI(env=aws_env)


@pytest.fixture(scope='session', autouse=True)
def clean_snapshots(cornac_env):
    yield None
    if 'CORNAC_BACKUPS_LOCATION' not in cornac_env:
        logger.info("Snapshot disabled. Not cleaning.")
        return
    if 'KEEP' in os.environ:
        logger.info("Snapshot cleanup disabled by KEEP env var;")
        return

    location = urlparse(cornac_env['CORNAC_BACKUPS_LOCATION'])
    ssh(
        '-l', location.username, location.hostname,
        'rm', '-rvf', location.path,
    )


@pytest.fixture(scope='session')
def _maildir():
    """Temporary directory fixture to store sent emails."""
    with tempfile.TemporaryDirectory() as tmpdir:
        yield Path(tmpdir)


@pytest.fixture(scope='function')
def maildir(_maildir):
    """Fixture handling cleanup of mail directory at function scope."""
    yield _maildir
    for f in _maildir.iterdir():
        f.unlink()


@pytest.fixture(scope='session', autouse=True)
def etcd_patroni(app):
    if not app.has_etcd:
        yield None
        return

    try:
        yield None
    finally:
        prefix = app.config['MACHINE_PREFIX'].strip('-')
        with etcd_connect() as etcd:
            try:
                services = etcd.read('/service').children
            except EtcdKeyNotFound:
                services = []

            for service in services:
                cluster_name = os.path.basename(service.key)
                if cluster_name.startswith(prefix):
                    logger.info("Deleting key %s from etcd.", service.key)
                    etcd.delete(service.key, recursive=True)

            res = etcd.http_get('/auth/roles')
            roles = res.json()['roles']
            for role in roles:
                rolename = role['role']
                if rolename.startswith(prefix):
                    etcd.http_delete(f"/auth/roles/{rolename}")

            res = etcd.http_get('/auth/users')
            users = res.json()['users']
            for user in users:
                username = user['user']
                if username.startswith(prefix):
                    etcd.http_delete(f"/auth/users/{username}")


@pytest.fixture(scope='session')
def cornac_env(db: dict, _maildir):
    preserved_vars = set((
        'CORNAC_BACKUPS_LOCATION',
        'CORNAC_DEPLOY_KEY',
        'CORNAC_DNS_DOMAIN',
        'CORNAC_ETCD',
        'CORNAC_ETCD_CREDENTIALS',
        'CORNAC_IAAS',
        'CORNAC_MACHINE_ORIGIN',
        'CORNAC_NETWORK',
        'CORNAC_OPERATOR',
        'CORNAC_STORAGE_POOL_A',
        'CORNAC_STORAGE_POOL_B',
        'CORNAC_TEMBOARD',
        'CORNAC_TEMBOARD_CREDENTIALS',
        'CORNAC_TEST_VIRTUAL_IPS_RANGE',
        'CORNAC_VCENTER_RESOURCE_POOL_A',
        'CORNAC_VCENTER_RESOURCE_POOL_B',
    ))

    # Generate a clean environment for cornac commands.
    clean_environ = dict(
        (k, v) for k, v in os.environ.items()
        if k in preserved_vars or
        (not k.startswith('AWS_') and
         not k.startswith('PG') and
         not k.startswith('CORNAC_'))
    )

    location = clean_environ.get('CORNAC_BACKUPS_LOCATION')
    if location is not None:
        clean_environ['CORNAC_BACKUPS_LOCATION'] = \
            location.rstrip('/') + '/functests'

    # Reuse local prefix.
    prefix = os.environ.get('CORNAC_MACHINE_PREFIX', 'cornac-')
    origin = clean_environ.get('CORNAC_MACHINE_ORIGIN', f"{prefix}-origin")
    cornac_env = dict(
        clean_environ,
        CORNAC_CONFIG=str(Path(__file__).parent.parent / 'cornac-config.py'),
        CORNAC_ENV='development',
        CORNAC_MAIL_DSN=f'file://{_maildir.absolute()}',
        CORNAC_MAIL_FROM='PostgreSQL DBaaS test server <frank@acme.tld>',
        # Reuse origin with dev prefix.
        CORNAC_MACHINE_ORIGIN=origin,
        # Create machine with a test prefix.
        CORNAC_MACHINE_PREFIX=f"test{prefix}",
        CORNAC_SECRET_KEY='notasecret',
    )

    if db:
        host = db['PGHOST']
        port = db.get('PGPORT', '5432')
        user = db['PGUSER']
        password = db['PGPASSWORD']
        dbname = db['PGDATABASE']
    else:
        host = cornac("guess-endpoint-address", _env=cornac_env).strip()
        port = '5432'
        dbname = user = 'cornac'
        password = os.environ.get('PGPASSWORD', '1EstP4ss')

    conninfo = f"postgresql://{user}:{password}@{host}:{port}/{dbname}"

    return dict(
        cornac_env,
        PGDATABASE=dbname,
        PGHOST=host,
        PGPASSWORD=password,
        PGPORT=port,
        PGUSER=user,
        CORNAC_SQLALCHEMY_DATABASE_URI=conninfo,
    )


@pytest.fixture(scope='session')
def cornacc():
    return CornacClient(base_url='http://localhost:8001', verify=False)


class CornacClient(httpx.Client):
    def login(self):
        r = self.post(
            '/cornac/login',
            json=dict(username='remi.root@acme.tld', password='notasecret'))
        r.raise_for_status()
        return r


def http_wait(url):
    for _ in range(32):
        try:
            return httpx.get(url)
        except httpx.NetworkError:
            sleep(.1)
        except Exception as e:
            return e
    else:
        raise Exception("Failed to start HTTP server on time.")


@pytest.fixture(scope='session', autouse=True)
def session_iaas(app):
    if app.config['IAAS'].startswith('none+'):
        yield None
        return

    try:
        iaas = IaaS.connect(app.config['IAAS'], app.config)
    except Exception as e:
        # Re-raise to avoid leaking IAAS in locals.
        del app
        raise Exception(f"{e.__class__.__name__}: {e}") from None

    with iaas:
        yield iaas
        if 'KEEP' in os.environ:
            logger.info("VM cleanup disabled by KEEP env var.")
            return
        for machine in iaas.list_machines():
            logger.info("Deleting %s.", machine)
            iaas.delete_machine(machine)


@pytest.fixture(scope='session')
def iaas(session_iaas):
    if session_iaas is None:
        pytest.skip("None IaaS")

    yield session_iaas


@pytest.fixture(scope='session')
def pgconnect(iaas):
    return pgconnect_func


@contextmanager
def pgconnect_func(instance, **kw):
    kw = dict(
        host=instance['Endpoint']['Address'],
        port=instance['Endpoint']['Port'],
        user=instance['MasterUsername'],
        dbname=instance['MasterUsername'],
        **kw
    )
    with psycopg2.connect(**kw) as conn:
        with conn.cursor() as curs:
            yield curs


@pytest.fixture(scope='session')
def rds(cornac_env):
    # Ensure no other cornac is running.
    try:
        httpx.get('http://localhost:8001/rds/')
        raise Exception("cornac web already running.")
    except httpx.NetworkError:
        pass

    proc = Popen(["cornac", "--verbose", "serve"], env=cornac_env)
    http_wait('http://localhost:8001/rds/')

    # Ensure cornac is effectively running.
    if proc.poll():
        raise Exception("Failed to start cornac web.")

    try:
        yield proc
    finally:
        proc.terminate()
        proc.wait()


@pytest.fixture(autouse=True)
def reset_logs(caplog, capsys):
    logging.getLogger('sh').setLevel(logging.ERROR)
    caplog.clear()
    capsys.readouterr()


def lazy_write(attr, data):
    # Lazy access sys.{stderr,stdout} to mix with capsys.
    getattr(sys, attr).write(data)
    return False  # should_quit


@pytest.fixture(scope='session', autouse=True)
def sh_fixes():
    import sh
    mod = sh._SelfWrapper__self_module
    # Always write outerr to current test capsys.
    mod.Command._call_args.update(dict(
        err=partial(lazy_write, 'stderr'),
        out=partial(lazy_write, 'stdout'),
        tee=True,
    ))
    # Don't truncate stderr on error.
    mod.ErrorReturnCode.__init__.__defaults__ = (False,)


@pytest.fixture(scope='session')
def session_worker(session_iaas, cornac_env):
    proc = Popen(["cornac", "--verbose", "worker"], env=cornac_env)
    try:
        yield proc
    finally:
        proc.terminate()
        proc.communicate()


@pytest.fixture
def temboardc(temboardc_session):
    if temboardc_session is None:
        pytest.skip()

    return temboardc_session


@pytest.fixture(scope='session', autouse=True)
def temboardc_session(app):
    if app.config['TEMBOARD'] is None:
        # Let test-scoped fixture skip, becase this session-wide fixture is in
        # autouse for cleanup.
        yield None
        return

    with TemBoard() as client:
        client.login()
        try:
            yield client
        finally:
            if 'KEEP' in os.environ:
                logger.info("temBoard cleanup disabled by KEEP env var;")
                return

            prefix = app.config['MACHINE_PREFIX'].strip('-')[:6] + '-'
            r = client.get_all_role_groups()
            for group in r.json()['groups']:
                if group['name'].startswith(prefix):
                    client.delete_role_group(group['name'])

            r = client.get_all_instance_groups()
            for group in r.json()['groups']:
                if group['name'].startswith(prefix):
                    client.delete_instance_group(group['name'])


@pytest.fixture(scope='session')
def worker(session_worker):
    yield session_worker
