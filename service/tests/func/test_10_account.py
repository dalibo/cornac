def test_describe_organization(aws, rds):
    out = aws("organizations", "describe-organization")
    org = out['Organization']
    assert 'remi.root@acme.tld' == org['MasterAccountEmail']
    assert org['MasterAccountId'] in org['MasterAccountArn']
    assert org['MasterAccountId'] in org['Arn']


def test_create_account(aws, rds, worker):
    out = aws(
        "organizations", "create-account",
        "--account-name", "Test",
        "--email", "testadmin@acme.tld",
    )

    status = out['CreateAccountStatus']

    assert 'Test' == status['AccountName']
    assert 'SUCCEEDED' in status['State']


def test_list_accounts(aws, rds):
    out = aws("organizations", "list-accounts")

    accounts = out['Accounts']
    assert len(accounts) > 1

    master = accounts[0]
    assert 'Master' == master['Name']
    assert '000000000001' == master['Id']
    assert 'ACTIVE' == master['Status']
    assert 'CREATED' == master['JoinedMethod']
    assert '@' in master['Email']
    assert master['JoinedTimestamp'] > 1000

    test = accounts[-1]

    assert 'Test' == test['Name']

    aws.wait_account(account_id=test['Id'])


def test_remove_account(aws, rds):
    out = aws(
        "organizations", "remove-account-from-organization",
        "--account-id", "000000000004",
    )

    assert not out


def test_assume_role(aws, cornacc, rds):
    cornacc.post(
        '/cornac/login',
        json=dict(username='remi.root@acme.tld', password='notasecret'))
    prod = cornacc.get('/cornac/accounts/000000000002')
    arn = prod.json()['account']['AdminRoleArn']
    out = aws(
        'sts', 'assume-role',
        '--role-arn', arn,
        '--role-session-name', 'unused',
    )

    assumed_aws = aws.with_credentials(
        out['Credentials']['AccessKeyId'],
        out['Credentials']['SecretAccessKey'],
    )

    out = assumed_aws('sts', 'get-caller-identity')
    assert arn == out['Arn']

    # There is not instances in this account.
    out = assumed_aws('rds', 'describe-db-instances')
    assert 0 == len(out['DBInstances'])
