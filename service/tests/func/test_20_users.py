import re

import httpx
import pytest
from sh import ErrorReturnCode


def test_access_key_lifecycle(aws, rds, aws_env, capsys):
    aws = aws.profile('other')

    # Create secondary access.
    out = aws('iam', 'create-access-key')

    secondary_access_env = dict(
        aws_env,
        AWS_ACCESS_KEY_ID=out['AccessKey']['AccessKeyId'],
        AWS_SECRET_ACCESS_KEY=out['AccessKey']['SecretAccessKey'],
    )
    # Use it to list access key.
    out = aws('iam', 'list-access-keys', _env=secondary_access_env)
    assert 2 == len(out['AccessKeyMetadata'])

    # Ensure access key quota is reached.
    with pytest.raises(ErrorReturnCode) as errinfo:
        aws('iam', 'create-access-key')
    assert 'LimitExceeded' in errinfo.value.stderr

    # Deactivate secondary key.
    aws('iam', 'update-access-key',
        '--access-key-id', secondary_access_env['AWS_ACCESS_KEY_ID'],
        '--status', 'Inactive')

    # Ensure it's now invalid
    with pytest.raises(ErrorReturnCode) as errinfo:
        aws('iam', 'list-access-keys', _env=secondary_access_env)
    assert 'InvalidClientTokenId' in errinfo.value.stderr

    # Reactivate secondary key.
    aws('iam', 'update-access-key',
        '--access-key-id', secondary_access_env['AWS_ACCESS_KEY_ID'],
        '--status', 'Active')
    aws('iam', 'list-access-keys', _env=secondary_access_env)

    # Delete it
    aws('iam', 'delete-access-key',
        '--access-key-id', secondary_access_env['AWS_ACCESS_KEY_ID'])


user_args = ['--user-name', 'thomas.tmp@acme.tld']


def test_create_user(aws, rds):
    aws('iam', 'create-user', *user_args)

    out = aws('iam', 'get-user', *user_args)
    user = out['User']
    assert 'UserId' in user
    assert 'thomas.tmp@acme.tld' == user['UserName']


def test_password_lifecycle_admin(aws, cornacc, rds):
    # Define password.
    with pytest.raises(ErrorReturnCode) as errinfo:
        aws('iam', 'get-login-profile', *user_args)
    assert 'NoSuchEntity' in errinfo.value.stderr

    out = aws('iam', 'create-login-profile', *user_args,
              '--password', 'totoprofile')
    assert 'CreateDate' in out['LoginProfile']

    # Try password.
    def login(username='thomas.tmp@acme.tld', *, password):
        r = cornacc.post(
            '/cornac/login',
            json=dict(username=username, password=password))
        r.raise_for_status()
        return r

    with pytest.raises(httpx.HTTPError):
        login(password='bad password')
    login(password='totoprofile')

    # Change password.
    aws('iam', 'update-login-profile', *user_args, '--password', 'profupdate')
    login(password='profupdate')

    # Drop password.
    aws('iam', 'delete-login-profile', *user_args)
    with pytest.raises(httpx.HTTPError):
        login(password='peu importe')


def test_password_lifecycle_user(cornac_env, maildir, aws, cornacc, rds):
    username = user_args[1]
    r = cornacc.post('/cornac/reset_password/', json=dict(username=username))
    r.raise_for_status()

    msgfile, = list(maildir.iterdir())
    with open(msgfile) as f:
        msg = f.read()
    m = re.search(r"http://.+:8001(/.+)\n", msg)
    assert m is not None, "Failed to find reset password URL"
    path = m.group(1)
    path = path.replace('cornac/#/', 'cornac/')

    r = cornacc.post(
        path,
        json=dict(password='totoreset'),
    )
    r.raise_for_status()

    r = cornacc.post(
        '/cornac/login',
        json=dict(username=username, password='totoreset'))
    r.raise_for_status()
    creds = r.json()['credentials']

    r = cornacc.post(
        '/cornac/login/renew',
        json=dict(session_token=creds['SessionToken']),
    )
    r.raise_for_status()
    creds = r.json()['credentials']

    tmp_aws = aws.with_credentials(
        creds['AccessKeyId'],
        creds['SecretAccessKey'],
    )

    tmp_aws(
        'iam', 'change-password',
        '--old-password', 'totoreset',
        '--new-password', 'totochanged',
    )

    r = cornacc.post(
        '/cornac/login',
        json=dict(username=username, password='totochanged'))
    r.raise_for_status()


def test_rename_user(aws, rds):
    aws('iam', 'update-user',
        *user_args,
        '--new-user-name', 'thierry.tmp@acme.tld')

    # Update global for next tests
    user_args[1] = 'thierry.tmp@acme.tld'


def test_list_groups(aws, rds):
    out = aws('iam', 'list-groups')
    names = [m['GroupName'] for m in out['Groups']]
    assert 'Admins' in names


def test_add_remote_in_group(aws, rds):
    group_args = ['--group-name', 'Admins']

    out = aws('iam', 'list-groups-for-user', *user_args)
    assert 0 == len(out['Groups'])

    aws('iam', 'add-user-to-group', *user_args, *group_args)

    out = aws('iam', 'list-groups-for-user', *user_args)
    assert 1 == len(out['Groups'])

    aws('iam', 'remove-user-from-group', *user_args, *group_args)

    out = aws('iam', 'list-groups-for-user', *user_args)
    assert 0 == len(out['Groups'])


def test_delete_user(aws, rds):
    out = aws('iam', 'list-users')
    pre_count = len(out['Users'])
    aws('iam', 'delete-user', *user_args)

    out = aws('iam', 'list-users')
    post_count = len(out['Users'])

    assert pre_count > post_count


def test_policy(aws, rds):
    uarn = 'arn:cornac:iam::000000000001:user/remi.root@acme.tld'
    res = aws(
        'iam', 'simulate-principal-policy',
        '--policy-source-arn', uarn,
        '--action-names', 'rds:CreateDBInstance',
    )
    res = res['EvaluationResults']

    assert 'allowed' == res[0]['EvalDecision']

    uarn = 'arn:cornac:iam::000000000001:user/odile.other@acme.tld'
    res = aws(
        'iam', 'simulate-principal-policy',
        '--policy-source-arn', uarn,
        '--action-names', 'rds:CreateDBInstance',
    )
    res = res['EvaluationResults']

    assert 'implicitDeny' == res[0]['EvalDecision']
