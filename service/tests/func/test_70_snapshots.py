import os

import pytest


if 'CORNAC_BACKUPS_LOCATION' not in os.environ:
    pytest.skip('No backup location.', allow_module_level=True)


def test_create_snapshot(aws, rds, worker):
    out = aws(
        "rds", "create-db-snapshot",
        "--db-instance-identifier", "cornac",
        "--db-snapshot-identifier", "cornac-manual",
    )
    s = out['DBSnapshot']

    assert 'cornac-manual' == s['DBSnapshotIdentifier']
    assert 'creating' == s['Status']
    assert 'cornac' == s['DBInstanceIdentifier']
    assert 'local1a' == s['AvailabilityZone']


def test_describe_and_wait_snapshots(aws, rds, worker):
    out = aws("rds", "describe-db-snapshots")
    assert 2 == len(out['DBSnapshots'])

    # Tests filter on DBSnapshotIdentifier.
    aws.wait_snapshot(identifier='cornac-manual')


def test_restore_from_snapshot(aws, rds, worker):
    out = aws(
        "rds", "restore-db-instance-from-db-snapshot",
        "--db-snapshot-identifier", "cornac-manual",
        "--db-instance-identifier", "cornac-rest-manual",
    )
    i = out['DBInstance']
    assert 'cornac-rest-manual' == i['DBInstanceIdentifier']
    assert 'creating' == i['DBInstanceStatus']
    assert 'local1a' == i['AvailabilityZone']


def test_restore_to_point_in_time(aws, rds, worker):
    out = aws(
        "rds", "restore-db-instance-to-point-in-time",
        "--source-db-instance-identifier", "cornac",
        "--target-db-instance-identifier", "cornac-rest-pitr",
        "--use-latest-restorable-time",
        "--availability-zone", "local1b",
    )
    i = out['DBInstance']
    assert 'cornac-rest-pitr' == i['DBInstanceIdentifier']
    assert 'creating' == i['DBInstanceStatus']
    assert 'local1b' == i['AvailabilityZone']


def test_wait_and_check_restored_instances(aws, rds, worker):
    aws.wait_instance(identifier='cornac-rest-manual', fail='restore-error')
    aws.wait_instance(identifier='cornac-rest-pitr', fail='restore-error')

    out = aws("rds", "describe-db-snapshots")
    # 1 manual + 3 automated
    assert 4 == len(out['DBSnapshots'])


def test_delete_snapshot(aws, rds, worker):
    out = aws(
        "rds", "delete-db-snapshot",
        "--db-snapshot-identifier", "cornac-manual",
    )
    s = out['DBSnapshot']

    assert 'cornac-manual' == s['DBSnapshotIdentifier']
    assert 'deleted' == s['Status']
    assert 'cornac' == s['DBInstanceIdentifier']

    aws.wait_snapshot(None, identifier='cornac-manual')


def test_delete_instances(aws, rds, worker):
    aws(
        "rds", "delete-db-instance",
        "--db-instance-identifier", "cornac-rest-manual",
        "--final-db-snapshot-identifier", "cornac-rest-manual-final")
    aws(
        "rds", "delete-db-instance",
        "--db-instance-identifier", "cornac-rest-pitr",
        "--skip-final-snapshot",
        "--no-delete-automated-backups",
    )

    aws.wait_instance(identifier='cornac-rest-manual', wanted='__deleted__')
    aws.wait_instance(identifier='cornac-rest-pitr', wanted='__deleted__')

    out = aws(
        "rds", "describe-db-snapshots",
        "--db-instance-identifier", "cornac-rest-manual")
    assert 1 == len(out['DBSnapshots'])

    aws(
        "rds", "delete-db-snapshot",
        "--db-snapshot-identifier", "cornac-rest-manual-final")
    aws.wait_snapshot(
        identifier="cornac-rest-manual-final", wanted='__deleted__')

    out = aws(
        "rds", "describe-db-snapshots",
        "--db-instance-identifier", "cornac-rest-pitr",
        "--snapshot-type", "automated",
    )
    assert len(out['DBSnapshots']) > 0

    for snapshot in out['DBSnapshots']:
        aws(
            "rds", "delete-db-snapshot",
            "--db-snapshot-identifier", snapshot['DBSnapshotIdentifier'])
        aws.wait_snapshot(
            identifier=snapshot['DBSnapshotIdentifier'], wanted='__deleted__')
