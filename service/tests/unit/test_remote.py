def test_password():
    from cornac.ssh import Password

    my = Password('secret')

    assert 'secret' not in str(my)
    assert 'secret' not in repr(my)


def test_url():
    from cornac.ssh import RemoteShell

    url = 'ssh://user@host:2222/path'

    shell = RemoteShell.from_url(url)

    assert "2222" in shell.ssh
    assert "user" in shell.ssh
    assert "host" in shell.ssh
