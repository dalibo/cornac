import pytest


def test_creation():
    from cornac.core.instanceclass import DBInstanceClass

    class_ = DBInstanceClass(cpu=1, xeon=True)
    assert 'xeon' in repr(class_)
    assert 'cpu=1' in repr(class_)
    assert 'db.cpu1.xeon' == str(class_)


def test_serialisation():
    from cornac.core.instanceclass import DBInstanceClass

    class_ = DBInstanceClass.parse('db.cpu1.xeon.mem05')

    assert 1 == class_.cpu
    assert 0.5 == class_.mem
    assert class_.xeon is True
    assert class_.alias is None

    assert 'db.cpu1.xeon.mem05' == str(class_)
    assert 'xeon' in repr(class_)

    with pytest.raises(ValueError):
        # missing db.
        DBInstanceClass.parse('cpu1')

    with pytest.raises(ValueError):
        # Inverted name and value.
        DBInstanceClass.parse('db.1cpu')


def test_alias():
    from cornac.core.instanceclass import DBInstanceClass

    class_ = DBInstanceClass.parse('db.t2.micro')

    assert hasattr(class_, 'cpu')
    assert hasattr(class_, 'mem')
    assert not hasattr(class_, 't')
    assert not hasattr(class_, 'micro')

    assert 'db.t2.micro' == str(class_)
    assert 'db.t2.micro' == class_.alias
