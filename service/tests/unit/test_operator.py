import pytest


def test_factory(mocker):
    iep = mocker.patch('cornac.operator.base.iter_entry_points', spec=True)
    from cornac.operator.base import Operator, KnownError

    iep.return_value = []
    with pytest.raises(KnownError):
        Operator.factory(None, config={'OPERATOR': 'pouet'})

    ep = mocker.Mock(name='entrypoint')
    iep.return_value = [ep]
    operator = Operator.factory(None, config={'OPERATOR': 'pouet'})

    assert ep.load.called is True
    assert operator is ep.load.return_value.return_value
