import pytest
from conftest import CornacClient
from flask import Response


def test_array():
    from cornac.web.endpoint import resolve_payload_arrays

    payload = {
        'Key0': 0,
        'ArrayA.member.1': 'a1',
        'ArrayA.member.2': 'a2',
        'ArrayB.member.1': 'b1',
    }

    payload = resolve_payload_arrays(payload)
    assert 'ArrayA' in payload
    assert 'ArrayA.member.1' not in payload
    assert ['a1', 'a2'] == payload['ArrayA']
    assert ['b1'] == payload['ArrayB']

    with pytest.raises(ValueError):
        resolve_payload_arrays({'Array.member.3': 'plop'})


def test_virtual_ip_get(client: CornacClient, mocker):
    resp: Response = client.get('/cornac/virtual-ip-ranges')

    assert 403 == resp.status_code

    client.access_key = client.fake_access_key
    client.acl_result = client.ALLOW
    VirtualIPRange = mocker.patch('cornac.web.cornac.rds.VirtualIPRange')

    # Mock database.
    VirtualIPRange.query.all.return_value = ranges = []
    range_ = mocker.Mock(name='VirtualIPRange()')
    range_.as_dict.return_value = dict(id=1)
    ranges.append(range_)

    resp: Response = client.get('/cornac/virtual-ip-ranges')

    assert 200 == resp.status_code
    assert 'ranges' in resp.json
    assert 1 == len(resp.json['ranges'])
    assert range_.as_dict.called is True


def test_virtual_ip_post(client: CornacClient, mocker):
    VirtualIPRange = mocker.patch('cornac.web.cornac.rds.VirtualIPRange')
    mocker.patch('cornac.web.cornac.rds.db.session')

    client.access_key = client.fake_access_key
    client.acl_result = client.ALLOW

    resp: Response = client.post('/cornac/virtual-ip-ranges')

    assert 400 == resp.status_code
    assert 'error' in resp.json

    resp: Response = client.post(
        '/cornac/virtual-ip-ranges', json=dict(range='bad'),
    )

    assert 400 == resp.status_code
    assert 'error' in resp.json

    resp: Response = client.post(
        '/cornac/virtual-ip-ranges', json=dict(range='10.0.0.0/25'),
    )

    assert 400 == resp.status_code
    assert 'error' in resp.json

    VirtualIPRange.query.filter.return_value.first.return_value = None
    range_ = VirtualIPRange.return_value
    range_.as_dict.return_value = dict(id=1)

    resp: Response = client.post(
        '/cornac/virtual-ip-ranges', json=dict(range='10.0.0.0/25'),
    )

    assert 200 == resp.status_code
    assert 'range' in resp.json
