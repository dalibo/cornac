import pytest


def test_create_db_instance(app):
    from cornac.web.rds import checks
    from cornac.web.rds import errors

    @checks.CreateDBInstance
    def handler(**parameters):
        return parameters

    def run(defaults=None, **parameters):
        if defaults:
            parameters = dict(defaults, **parameters)
        with app.test_request_context():
            return handler(**parameters)

    minimal = dict(
        AllocatedStorage='8',
        DBInstanceClass='db.cpu1.mem2',
        DBInstanceIdentifier='test',
        Engine='postgres',
        MasterUsername='toto',
        MasterUserPassword='totoT0T0',
    )

    parameters = run(
        minimal,
        AvailabilityZone='local1a',
        EnablePerformanceInsights='true',
        EngineVersion='13',
        MultiAZ='false',
        UnknownParameter='toto',
    )

    assert 'UnknownParameter' not in parameters
    assert 'PerformanceInsightsEnabled' in parameters
    assert 'EnablePerformanceInsights' not in parameters
    assert parameters['MultiAZ'] is False

    with pytest.raises(errors.InvalidParameterValue):
        run()

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, MasterUsername='')

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, DBInstanceClass='pouet')

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, DBInstanceIdentifier='000--')

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, Engine='oracle')

    with pytest.raises(errors.InvalidParameterCombination):
        run(minimal, EngineVersion='10.1000')

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, MultiAZ='pouet')

    with pytest.raises(errors.InvalidParameterCombination):
        run(minimal, MultiAZ='true', AvailabilityZone='local1a')

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, MasterUsername='postgres')

    with pytest.raises(errors.InvalidParameterValue):
        run(minimal, MasterUserPassword='simple')
