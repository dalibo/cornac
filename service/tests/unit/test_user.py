from datetime import datetime


def test_send_reset_password_email(app):
    from cornac.core.user import send_reset_password_email

    expiration_date = datetime(2020, 5, 20, 12, 49, 34)
    with app.test_request_context():
        send_reset_password_email(
            app,
            'bob@example.com',
            expiration_date,
            'https://corn.ac/reset/xyz',
        )
    assert app.mail.outbox
    msg, = app.mail.outbox
    assert msg.from_email == 'cornac@example.com'
    assert msg.recipients() == ['bob@example.com']
    assert 'Reset your password at https://corn.ac/reset/xyz' in msg.body
    assert 'until 20/05/2020 12:49 UTC.' in msg.body
