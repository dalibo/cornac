from unittest.mock import patch, Mock

import pytest
from flask import g
from flask.testing import FlaskClient

from cornac import create_app
from cornac.core.model import AccessKey, Account, Identity
from cornac.web.errors import AWSError


@pytest.fixture(scope='session')
def app():
    env = dict(
        # Mock ssh key.
        CORNAC_DEPLOY_KEY='ssh-ed25519 dumb-deploy-key user@host',
        CORNAC_MAIL_DSN='memory:',
        CORNAC_MAIL_FROM='cornac@example.com',
        CORNAC_SQLALCHEMY_DATABASE_URI='sqlite:///:memory:'
    )
    # Skip loading .env
    dv = Mock(name='dotenv_value')
    dv.return_value = dict()
    with patch('cornac.core.config.dotenv_values', dv):
        app = create_app(environ=env)

    app.testing = True
    app.test_client_class = CornacClient

    return app


@pytest.fixture(scope='session')
def client(app):
    with app.test_client() as client:
        yield client


class CornacClient(FlaskClient):
    # Fake accesskey to use as self.access_key result
    fake_access_key = AccessKey(identity=Identity(account=Account(id=1)))
    ALLOW = object()
    DENY = object()

    def __init__(self, *a, **kw):
        super(CornacClient, self).__init__(*a, **kw)
        self.access_key = False  # Triggers authentication.
        self.acl_result = None  # None, ALLOW or DENY

    def authenticate(self):
        # Fake authentication, instead of reading session or security token,
        # inject self.access_key.
        g.access_key = self.access_key
        if self.access_key:
            g.current_account = g.access_key.account
            g.current_identity = g.access_key.identity
        else:
            raise AWSError("No access_key")

    def open(self, *a, **kw):
        mod = 'cornac.web.cornac.auth.'
        with patch(mod + 'make_signer'), \
                patch(mod + 'authenticate_from_session', self.authenticate), \
                patch('cornac.web.auth.ACL') as ACL:

            # Inject ACL statement according to requested success of check().
            ACL.query.match.return_value.all.return_value = statements = []
            if self.acl_result is self.ALLOW:
                statements.append(Mock(name='statement', effect='Allow'))
            elif self.acl_result is self.DENY:
                statements.append(Mock(name='statement', effect='Deny'))

            if self.access_key:
                headers = kw.setdefault('headers', {})
                headers['X-Amz-Security-Token'] = 'MOCKED'

            return super(CornacClient, self).open(*a, **kw)
