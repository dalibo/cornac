import os
import secrets
import string
from base64 import b64encode
from hashlib import pbkdf2_hmac
from textwrap import dedent

from flask import current_app
from flask_mailman import Message


_ACCESS_KEY_LETTERS = string.ascii_uppercase + string.digits
_SECRET_LETTERS = string.ascii_letters + string.digits + '+/-_'


def generate_id(prefix='CKIC', length=20):
    # AKDC stands for Access Key Dalibo Cornac.
    return prefix + ''.join(secrets.choice(_ACCESS_KEY_LETTERS)
                            for _ in range(length - len(prefix)))


def generate_secret(length=40):
    return ''.join(secrets.choice(_SECRET_LETTERS) for _ in range(length))


def generate_session_token(length=258):
    return b64encode(os.urandom(length)).decode('ascii')


def hash_password(password, salt):
    return b64encode(pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),
        salt.encode('utf-8'),
        1_000_000,
    )).decode('ascii')


def send_reset_password_email(app, recipient, expiration_date, formurl):
    product_name = "PostgreSQL DBaaS"
    body = dedent(f"""
    Hello,

    You recently requested to reset your password for your {product_name}
    account. Use the link below to reset it. This password reset is valid
    until {expiration_date.strftime('%d/%m/%Y %H:%M UTC')}.

    Reset your password at {formurl}

    If you did not request a password reset, please ignore this email.

    Thanks,
    The {product_name} Team
    """)
    msg = Message(
        subject=f"{product_name} Password Reset",
        body=body,
        sender=current_app.config['MAIL_FROM'],
        recipients=[recipient],
    )
    app.mail.send(msg)
    app.logger.info("Password reset email sent for %s.", recipient)
