import logging
from textwrap import dedent

from flask import current_app

from ..errors import KnownError
from ..utils import canonical_url_for, check_url


logger = logging.getLogger(__name__)


def build_metadata(hostname):
    return {
        "instance-id": hostname,
        "local-hostname": hostname,
        "public-keys": [current_app.config['DEPLOY_KEY']],
    }


def build_userdata(hostname):
    return dedent(f"""\
    #cloud-config
    disable_root: false
    hostname: {hostname}
    ssh_authorized_keys:
    - "{current_app.config['DEPLOY_KEY']}"
    runcmd:
    # Refresh DHCP configuration to tell DHCP server the current hostname.
    - systemctl restart NetworkManager
    final_message: "Initialized by Cornac."
    """)


def build_seedfrom_url(name):
    metadata_baseurl = canonical_url_for(
        "cornac.cloudinit_metadata", hostname=name)
    try:
        check_url(metadata_baseurl)
    except KnownError:
        logger.warning("Is cornac-web running?")
        raise
    return metadata_baseurl.replace('meta-data', '')
