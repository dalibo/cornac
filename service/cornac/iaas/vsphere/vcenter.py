#
# IaaS provider on top of vSphere API
#
# cf. https://code.vmware.com/apis/358/vsphere
#

import logging
import os.path
import json
from contextlib import contextmanager
from pathlib import Path
from ssl import SSLError
from urllib.parse import (
    parse_qs,
    urlparse,
)
from base64 import b64encode

import tenacity
from pyVim.connect import (
    Disconnect,
    SmartConnect,
    SmartConnectNoSSL,
)
from pyVmomi import (
    vim,
    vmodl,
)

from .. import IaaS
from ...errors import (
    KnownError,
    RemoteCommandError,
)
from ...ssh import RemoteShell
from ...core.cloudinit import (
    build_metadata,
    build_userdata,
)
from ...operator.machine import Machine


logger = logging.getLogger(__name__)


@contextmanager
def lint_error():
    # pyVmomi errors string serialization is ugly. This context manager reraise
    # errors with clean message.
    try:
        yield None
    except vmodl.MethodFault as e:
        raise Exception(e.msg) from e


# Retryier for operation on vCenter when vCenter server loose connection to
# ESXi host.
retry_esx_connection = tenacity.retry(
    after=tenacity.after_log(logger, logging.DEBUG),
    reraise=True,
    retry=(tenacity.retry_if_exception_type(vim.fault.InvalidHostState) |
           tenacity.retry_if_exception_type(vmodl.fault.HostNotConnected)),
    stop=tenacity.stop_after_attempt(5),
    wait=tenacity.wait_fixed(1),
)


class vCenter(IaaS):
    @classmethod
    def connect(cls, url, config):
        url = urlparse(url)
        args = parse_qs(url.query)
        no_verify = args.get('no_verify', ['0']) == ['1']
        connector = SmartConnectNoSSL if no_verify else SmartConnect
        logging.debug(
            "Connecting to vCenter %s@%s.", url.username, url.hostname)
        with lint_error():
            try:
                si = connector(
                    host=url.hostname,
                    user=url.username,
                    pwd=url.password,
                    port=url.port or 443,
                )
            except SSLError as e:
                if 'CERTIFICATE_VERIFY_FAILED' == e.reason and not no_verify:
                    logger.error("Bad SSL certificate to vCenter Server.")
                    logger.error(
                        "Do you need no_verify=1 parameter to vCenter URL?")
                raise
        return cls(si, config)

    def __init__(self, si, config):
        self.config = config
        # si stands for ServiceInstance.
        self.si = si

    def __repr__(self):
        return '<%s at %s>' % (
            self.si.content.about.fullName,
            self.si._stub.host,
        )

    @retry_esx_connection
    def clone_machine(self, origin, name, clonespec):
        logger.debug("Cloning %s as %s.", origin.name, name)
        task = origin.Clone(folder=origin.parent, name=name, spec=clonespec)
        return self.wait_task(task)

    def close(self):
        logger.debug("Disconnecting from vSphere.")
        Disconnect(self.si)
        self.si = None

    def create_machine(
            self, machine, instance_class, data_size_gb=None):
        name = machine.hostname
        logger.debug("Creating %s specification.", name)
        storage_pool = machine.zoned_config('STORAGE_POOL_')
        try:
            datastore = self.find(storage_pool)
        except KeyError:
            raise KnownError(f"Unknown datastore {storage_pool}.")
        try:
            origin = self.find(self.origin)
        except KeyError:
            raise KnownError(f"Missing origin VM {self.origin}.")

        clonespec = vim.vm.CloneSpec()
        clonespec.powerOn = True
        clonespec.config = vim.vm.ConfigSpec()
        clonespec.config.annotation = "Postgres machine managed by cornac."
        clonespec.config.extraConfig = build_cloudinit_metadata(machine)
        # Map AWS Memory (GiB) to vSphere memory (MiB)
        clonespec.config.memoryMB = int(instance_class.mem * 1024)
        clonespec.config.numCPUs = instance_class.cpu
        clonespec.config.deviceChange.append(
            build_data_disk_spec(origin, datastore, name, data_size_gb)
        )
        clonespec.location = locspec = vim.vm.RelocateSpec()
        locspec.datastore = datastore
        respool = machine.zoned_config('VCENTER_RESOURCE_POOL_')
        try:
            locspec.pool = self.find(respool)
        except KeyError:
            raise KnownError(f"Unknown resource pool {respool}.")
        locspec.deviceChange.append(build_nic_spec(self.config['NETWORK']))

        if len(origin.rootSnapshot):
            snapshot_tree = find_snapshot_tree(
                origin.snapshot.rootSnapshotList,
                origin.snapshot.currentSnapshot,
            )
            clonespec.snapshot = snapshot_tree.snapshot
            logger.debug(
                "Using linked clone from %s %s.",
                snapshot_tree.snapshot, snapshot_tree.name)
            locspec.diskMoveType = 'createNewChildDiskBacking'
        else:
            logger.debug("Full clone from %s.", origin)

        logger.info("Creating machine %s.", name)
        try:
            vm = self.clone_machine(origin, name, clonespec)
        except vim.fault.DuplicateName:
            raise KnownError(f"VM {name} already exists.")

        return vm

    @retry_esx_connection
    def delete_machine(self, machine):
        try:
            vm = self._ensure_vm(machine)
        except KnownError as e:
            return logger.debug("%s machine undefined: %s", machine, e)

        if self.is_running(vm):
            logger.info("Powering off %s.", vm)
            with self.wait_update(vm, 'runtime.powerState'):
                self.wait_task(vm.PowerOff())
        logger.info("Destroying %s.", vm)
        return self.wait_task(vm.Destroy_Task())

    def endpoint(self, machine):
        vm = self._ensure_vm(machine)
        return vm.name + self.config['DNS_DOMAIN']

    def find(self, path):
        obj = self.si.content.searchIndex.FindByInventoryPath(path)
        if obj is None:
            raise KeyError(path)
        return obj

    def guess_data_device_in_guest(self, machine):
        # It's quite hard to determine exact device inside linux guest from VM
        # spec. Let's assume things are simple and reproducible. cf.
        # https://communities.vmware.com/thread/298072
        return "/dev/sdb"

    def is_running(self, machine):
        try:
            vm = self._ensure_vm(machine)
        except KnownError as e:
            logger.debug("%s not running: %s", machine, e)
            return False
        return 'poweredOn' == vm.runtime.powerState

    def list_machines(self):
        origin = os.path.basename(self.origin)
        folder = self.find(os.path.dirname(self.origin))
        for vm in folder.childEntity:
            if origin == vm.name:
                continue

            if vm.name.startswith(self.prefix):
                yield vm

    def _ensure_vm(self, machine):
        if hasattr(machine, "hostname"):
            vmfolder = Path(self.origin).parent
            name = machine.hostname
            objpath = f"{vmfolder}/{name}"
            try:
                return self.find(objpath)
            except KeyError:
                raise KnownError(f"{objpath} does not exists in IaaS.")
        return machine

    def _ensure_tools(self, vm):
        if 'toolsOk' == vm.guest.toolsStatus:
            return

        with self.wait_update(vm, 'guest.toolsStatus'):
            logger.debug("Wait for tools to come up on %s.", vm)

        if 'toolsOk' != vm.guest.toolsStatus:
            msg = f"{vm} tools at state {vm.guest.toolsStatus}."
            raise KnownError(msg)

    @retry_esx_connection
    def start_machine(self, machine, wait_ssh=False, wait_tools=False):
        vm = self._ensure_vm(machine)
        if self.is_running(vm):
            logger.debug("%s is already powered.", vm)
        else:
            with self.wait_update(vm, 'runtime.powerState'):
                logger.info("Powering %s on.", vm)
                self.wait_task(vm.PowerOn())

        if wait_ssh:
            ssh = RemoteShell('root', self.endpoint(vm))
            ssh.wait()

        if wait_tools:
            self._ensure_tools(vm)

    @retry_esx_connection
    def stop_machine(self, machine):
        vm = self._ensure_vm(machine)
        if 'poweredOff' == vm.runtime.powerState:
            return logger.debug("Already stopped.")

        with self.wait_update(vm, 'runtime.powerState'):
            shut = False
            if 'toolsOk' != vm.guest.toolsStatus:
                # If tools are slow to come up, try SSH before waiting for
                # tools. That may be faster.
                ssh = RemoteShell('root', self.endpoint(vm))
                logger.info("Shuting down %s through SSH.", vm)
                try:
                    ssh(["shutdown", "-h", "now"])
                except RemoteCommandError as e:
                    if e.connection_closed_by_remote:
                        # Connection closed, it's fine, let's wait powerOff.
                        shut = True
                    else:
                        logger.debug("SSH shutdown failed: %s.", e)

            if not shut:
                logger.info("Shuting down %s through vmTools.", vm)
                self._ensure_tools(vm)
                vm.ShutdownGuest()

    def iter_updates(self, obj, proppath, maxupdates=5):
        # Watch and yield first value and updates on one property of one
        # managed object.
        propSpec = vmodl.query.PropertyCollector.PropertySpec(
            type=type(obj), all=False, pathSet=[proppath])
        filterSpec = vmodl.query.PropertyCollector.FilterSpec(
            objectSet=[vmodl.query.PropertyCollector.ObjectSpec(obj=obj)],
            propSet=[propSpec])
        pc = self.si.content.propertyCollector
        waitopts = vmodl.query.PropertyCollector.WaitOptions(
            maxObjectUpdates=1, maxWaitSeconds=300)
        pcFilter = pc.CreateFilter(filterSpec, partialUpdates=True)

        try:
            update = pc.WaitForUpdatesEx('', options=waitopts)
            yield update.filterSet[0].objectSet[0]
            while maxupdates > 0:
                logger.debug("Waiting for update on %s.%s.", obj, proppath)
                update = pc.WaitForUpdatesEx(update.version, options=waitopts)
                yield update.filterSet[0].objectSet[0]
                maxupdates -= 1
        except GeneratorExit:
            pass
        finally:
            pcFilter.Destroy()

    @contextmanager
    def wait_update(self, obj, proppath):
        updates = self.iter_updates(obj, proppath)
        # First iteration generates current value.
        yield next(updates)
        # At exit, wait for next update before returning.
        next(updates)

    def wait_task(self, task):
        for update in self.iter_updates(task, 'info.state'):
            task = update.obj
            newstate = update.changeSet[0].val
            if newstate == vim.TaskInfo.State.success:
                return task.info.result
            elif newstate == vim.TaskInfo.State.error:
                raise task.info.error
            # Else, continue to wait.


def build_cloudinit_metadata(machine: Machine):
    options = {
        'guestinfo.userdata': (
            b64encode(build_userdata(machine.hostname).encode('utf-8'))
            # vim requires decoded data.
            .decode('utf-8')
        ),
        'guestinfo.userdata.encoding': 'base64',
        'guestinfo.metadata': (
            b64encode(
                json.dumps(build_metadata(machine.hostname))
                .encode('utf-8')
            )
            .decode('utf-8')
        ),
        'guestinfo.metadata.encoding': 'base64',
    }
    return [
        vim.option.OptionValue(key=k, value=v)
        for k, v in options.items()
    ]


def build_data_disk_spec(origin, datastore, name, size_gb):
    spec = vim.vm.device.VirtualDeviceSpec()
    spec.fileOperation = 'create'
    spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
    spec.device = disk = vim.vm.device.VirtualDisk()
    disk.capacityInKB = size_gb * 1024 * 1024
    scsi_controllers = list(filter(
        lambda d: hasattr(d, 'scsiCtlrUnitNumber'),
        origin.config.hardware.device
    ))
    disk.controllerKey = scsi_controllers[0].key
    disk.unitNumber = 1
    disks = list(filter(
        lambda d: hasattr(d.backing, 'diskMode'),
        origin.config.hardware.device
    ))
    disk.key = disks[-1].key + 1
    disk.unitNumber = disks[-1].unitNumber + 1
    disk.backing = backing = vim.vm.device.VirtualDisk.FlatVer2BackingInfo()  # noqa
    backing.fileName = f'[{datastore.name}] {name}/{name}-data.vmdk'
    backing.thinProvisioned = False
    backing.diskMode = 'persistent'
    return spec


def build_nic_spec(network):
    spec = vim.vm.device.VirtualDeviceSpec()
    spec.operation = vim.vm.device.VirtualDeviceSpec.Operation.add
    spec.device = nic = vim.vm.device.VirtualVmxnet3()
    nic.addressType = 'generated'
    nic.backing = vim.vm.device.VirtualEthernetCard.NetworkBackingInfo()
    nic.backing.useAutoDetect = False
    nic.backing.deviceName = network
    nic.connectable = vim.vm.device.VirtualDevice.ConnectInfo()
    nic.connectable.startConnected = True
    return spec


def find_snapshot_tree(snapshot_list, needle):
    # Find SnapshotTree object attached to snapshot needle from a snapshot
    # list. Metadata of a snapshot are defined in a snapshot tree node.
    for node in snapshot_list:
        if str(node.snapshot) == str(needle):
            return node
        try:
            return find_snapshot_tree(node.childSnapshotList, needle)
        except Exception:
            continue
    else:
        raise Exception(f"No node for snapshot {needle}")
