# a Temboard client for Python.

import re

import httpx
from flask import current_app


TemBoardError = httpx.HTTPError


class TemBoard:
    def __init__(self, url=None, verify=False):
        self.url = url or current_app.config['TEMBOARD']
        self.client = httpx.Client(base_url=self.url, verify=verify)
        self.username = None

    def __enter__(self):
        self.client.__enter__()
        return self

    def __exit__(self, *a):
        self.client.__exit__(*a)
        self.client = None
        self.username = None

    def __repr__(self):
        return '<%s %s %sconnected>' % (
            self.__class__.__name__,
            self.url,
            '' if self.client else 'dis',
        )

    def __getattr__(self, name):
        return getattr(self.client, name)

    def login(self, username=None, password=None):
        if not username:
            username, password = temboard_credentials()

        self.username = username

        r = self.client.post(
            "/login",
            data=dict(username=username, password=password),
            allow_redirects=False,
        )
        r.raise_for_status()
        return r

    def create_role_group(self, name, description):
        r = self.client.post(
            "/json/settings/group/role",
            json=dict(new_group_name=name, description=description),
        )
        r.raise_for_status()
        return r

    def create_instance_group(self, name, description, user_groups=None):
        r = self.client.post(
            "/json/settings/group/instance",
            json=dict(
                new_group_name=name,
                description=description,
                user_groups=user_groups or [],
            ),
        )
        r.raise_for_status()
        return r

    def delete_instance(self, address, port):
        r = self.client.post(
            "/json/settings/delete/instance",
            json=dict(agent_address=address, agent_port=port)
        )
        r.raise_for_status()
        return r

    def delete_instance_group(self, name):
        r = self.client.post(
            "/json/settings/delete/group/instance",
            json=dict(group_name=name)
        )
        r.raise_for_status()
        return r

    def delete_role_group(self, name):
        r = self.client.post(
            "/json/settings/delete/group/role",
            json=dict(group_name=name)
        )
        r.raise_for_status()
        return r

    def get_all_instance_groups(self):
        r = self.client.get("/json/settings/all/group/instance")
        r.raise_for_status()
        return r

    def get_all_role_groups(self):
        r = self.client.get("/json/settings/all/group/role")
        r.raise_for_status()
        return r

    def get_instance_group(self, name):
        r = self.client.get(f"/json/settings/group/instance/{name}")
        r.raise_for_status()
        return r

    def get_user(self, username):
        r = self.client.get("/json/settings/user/" + username)
        r.raise_for_status()
        return r

    def get_user_group(self, name):
        r = self.client.get(f"/json/settings/group/role/{name}")
        r.raise_for_status()
        return r

    def update_user(self, username, data):
        defaults = dict(
            new_username=username,
            password='',
            password2='',
        )
        data = dict(defaults, **data)
        r = self.client.post("/json/settings/user/" + username, json=data)
        r.raise_for_status()
        return r

    def update_instance_group(self, name, data):
        defaults = dict(
            new_group_name=name,
            user_groups=[],
        )
        data = dict(defaults, **data)
        r = self.client.post(
            "/json/settings/group/instance/" + name, json=data)
        r.raise_for_status()
        return r


def temboard_credentials():
    return current_app.config['TEMBOARD_CREDENTIALS'].split(':', 1)


def temboard_group_name(account):
    # temBoard group name must match ^([a-z0-9_\-.]{3,16})$.

    prefix = current_app.config['MACHINE_PREFIX'].strip('-')[:6]
    name = account.name[:6]
    group = f"{prefix}-{name}"
    group = group.lower()

    if not re.match(r'^([a-z0-9_\-.]{3,16})$', group):
        raise Exception(f"Can't build a temBoard group name for {group}.")

    return group
