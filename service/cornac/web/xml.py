from jinja2 import Environment, StrictUndefined


env = Environment(trim_blocks=True, undefined=StrictUndefined)


def booltostr(value):
    return 'true' if bool(value) is True else 'false'


env.filters['bool'] = booltostr


ERROR_TMPL = env.from_string("""\
<ErrorResponse>
   <Error>
      <Type>{{ type }}</Type>
      <Code>{{ awscode }}</Code>
      <Message>{{ message }}</Message>
   </Error>
   <RequestId>{{ requestid }}</RequestId>
</ErrorResponse>
""")


RESPONSE_TMPL = env.from_string("""\
<{{ action }}Response xmlns="{{ xmlns }}">
  <{{ action }}Result>
    {{ (result or '') | indent(4) }}
  </{{ action }}Result>
  <ResponseMetadata>
    <RequestId>{{ requestid }}</RequestId>
  </ResponseMetadata>
</{{ action }}Response>
""")
