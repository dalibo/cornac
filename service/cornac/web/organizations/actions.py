import logging

from flask import current_app
from sqlalchemy.exc import IntegrityError

from . import errors
from ..auth import check, check_access_to_rds
from ...core.model import db, Account
from ... import worker


logger = logging.getLogger(__name__)


def CreateAccount(AccountName, Email):
    check()

    try:
        account = Account.factory(name=AccountName, email=Email)
        db.session.commit()
        worker.create_account.send(account.id)
    except IntegrityError:
        raise errors.InvalidParameterValue(
            f"Account name {AccountName} already used.")

    return {"CreateAccountStatus": {
        "Id": "car-unused",
        "AccountName": account.name,
        "State": "SUCCEEDED",
        "RequestedTimestamp": account.data['JoinedTimestamp'],
    }}


def DescribeAccount(AccountId):
    try:
        AccountId = int(AccountId)
    except ValueError:
        raise errors.InvalidParameterValue("Invalid Account Id")

    account = Account.query.get(AccountId)
    if account is None:
        raise errors.AccountNotFoundException()

    check(resource=account)

    return {"Account": account.as_dict()}


def DescribeOrganization():
    master_account = Account.query.get_master()
    orgid = current_app.config['ORGANIZATION_ID']
    orgarn = f"arn:cornac:organizations::{master_account}:organization/{orgid}"
    check(resource=orgarn)
    return {"Organization": {
        "Arn": orgarn,
        "FeatureSet": "ALL",
        "Id": orgid,
        "MasterAccountArn": master_account.arn,
        "MasterAccountEmail": master_account.data['Email'],
        "MasterAccountId": f"{master_account}",
    }}


def ListAccounts():
    return {"Accounts": [
        a.as_dict()
        for a in Account.query.all() if
        check_access_to_rds(a)
    ]}


def RemoveAccountFromOrganization(AccountId):
    check()

    account = Account.query.get(int(AccountId))

    if account is None:
        raise errors.AccountNotFoundException("Account not found.")

    if account.is_master:
        raise errors.ConstraintViolationException(
            "Master account is protected.")

    account.status = 'XDELETING'
    db.session.commit()
    worker.delete_account.send(account.id)
