from flask import Blueprint

from . import actions
from ..endpoint import JSONEndpoint


blueprint = Blueprint('organizations', __name__)
endpoint = JSONEndpoint(
    namespace='organizations',
    version='2016-11-28',
    actions=actions,
)
endpoint.register(blueprint)
