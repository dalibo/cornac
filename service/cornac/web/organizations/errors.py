from ..errors import (
    AWSError,
    InvalidParameterCombination,
    InvalidParameterValue,
    NoSuchEntity,
    ValidationError,
)

__all__ = [
    'AWSError',
    'InvalidParameterCombination',
    'InvalidParameterValue',
    'NoSuchEntity',
    'ValidationError',
]


class AccountNotFoundException(AWSError):
    code = 400
    description = "You specified an account that doesn't exist."


class ConstraintViolationException(AWSError):
    code = 400
