import logging
from uuid import uuid4

from flask import current_app, g, request
from flask import make_response
from flask.json import jsonify
from sqlalchemy import exc as saerrors
from werkzeug.exceptions import HTTPException

from . import (
    errors,
    xml,
)
from .auth import authenticate


logger = logging.getLogger(__name__)


class ActionEndpoint:
    # Base class to implement either JSON or XML single POST request handler.
    #
    # To implement a new action, one must define a function in action module.
    # If the action requires background processing or worker privileges, one
    # must define a corresponding task (or actor) in cornac.worker. Finally, if
    # the action require effective operation on Postgres host or in IaaS, one
    # must add new methods in cornac.operator.Operator or its implementation,
    # as well as cornac.iaas.IaaS and its implementation.

    def __init__(self, namespace, version, actions):
        self.logger = logging.getLogger(__package__ + '.' + namespace)
        self.namespace = self.__name__ = namespace
        self.NAMESPACE = namespace.upper()
        self.version = version
        self.actions = actions

    def __call__(self):
        self.initialize_context()

        authenticate(request)
        action_name, payload = self.check_payload()

        try:
            action = getattr(self.actions, action_name)
        except AttributeError:
            self.logger.warning(
                "Unknown %s action: %s.", self.namespace, action_name)
            self.logger.debug("payload=%r", payload)
            raise errors.InvalidAction() from None
        else:
            action.__name__ = action_name

        result = action(**payload)

        # Post validatiton
        if g.last_acl_result is False:
            # This prevent information leaking, but not damage.
            logger.error(
                "Permission not checked by %s, refusing to answer.",
                action.__name__)
            raise errors.AccessDenied()

        # Render and log.
        response = self.make_response(result)
        self.log_response(response, 'OK')
        return response

    def check_payload(self):
        raise NotImplementedError

    def handle_error(self, e):
        if not isinstance(e, errors.AWSError):
            # Don't expose error, except HTTPException standard message.
            orig = e
            e = errors.AWSError()
            if isinstance(orig, HTTPException):
                e.code = e.code
                e.description = str(orig)
            elif isinstance(orig, saerrors.OperationalError):
                current_app.logger.error("Database error: %s", orig)
            else:
                current_app.logger.exception(
                    "Unhandled %s error:", self.NAMESPACE)
        response = self.make_response(e)
        self.log_response(response, e.awscode)
        return response

    def initialize_context(self):
        g.last_acl_result = False
        g.current_account = '-'
        # Used by c.w.auth and other.
        g.current_action = f"{self.namespace}:-"
        g.current_identity = '-'
        request.cornac_requestid = uuid4()

    def log_response(self, response, message):
        user = getattr(g.current_identity, 'name', g.current_identity)
        current_app.logger.log(
            logging.INFO if response.status_code < 400 else logging.ERROR,
            "%s %s %s %s %s",
            g.current_account, user, g.current_action,
            response.status_code, message,
        )

    def make_response(self, result):
        raise NotImplementedError

    def register(self, blueprint):
        blueprint.errorhandler(Exception)(self.handle_error)
        return blueprint.route('/', methods=['POST'])(self)


class JSONEndpoint(ActionEndpoint):
    def __init__(self, namespace, version, actions):
        super(JSONEndpoint, self).__init__(namespace, version, actions)
        self.AWSNamespace = 'AWS' + namespace.title()
        self.versionned_namespace = (
            self.AWSNamespace + 'V' + self.version.replace('-', ''))

    def initialize_context(self):
        super(JSONEndpoint, self).initialize_context()
        g.current_action = (
            request.headers.get('X-Amz-Target', '-')
            .replace(self.versionned_namespace + '.', self.namespace + ':'))

    def check_payload(self):
        force_json = request.mimetype == 'application/x-amz-json-1.1'
        request.get_json(force=force_json)
        try:
            target = request.headers['X-Amz-Target']
        except KeyError:
            raise errors.InvalidAction()
        namespace, action_name = target.split('.', 1)
        g.current_action = f'{self.namespace}:{action_name}'

        if not namespace.startswith(self.AWSNamespace):
            raise errors.InvalidAction(
                description=f"Unsupported API namespace {namespace}.")

        if namespace != self.versionned_namespace:
            _, version = namespace.split('V', 1)
            raise errors.InvalidAction(
                description=f"Unsupported API version {version}.")

        return action_name, request.json

    def make_response(self, result):
        # result is either an AWSError or a Python dict.
        status_code = 200
        if isinstance(result, errors.AWSError):
            e = result
            result = {
                '__type': e.__class__.__name__,
                'Message': e.description,
            }
            status_code = e.code

        response = jsonify(result)
        response.status_code = status_code
        if result is None:
            response.data = b''
        response.content_type = 'application/x-amz-json-1.1'
        response.headers['X-Amzn-RequestId'] = request.cornac_requestid
        return response


class XMLEndpoint(ActionEndpoint):
    def __init__(self, namespace, version, actions):
        super(XMLEndpoint, self).__init__(namespace, version, actions)
        self.xmlns = f"https://{namespace}.amazonaws.com/doc/{version}"

    def initialize_context(self):
        super(XMLEndpoint, self).initialize_context()
        g.current_action = (
            self.namespace + ':'
            + request.form.get('Action', '-'))

    def check_payload(self):
        payload = dict(request.form.items(multi=False))
        payload = resolve_payload_arrays(payload)
        try:
            action_name = payload.pop('Action')
        except KeyError:
            raise errors.InvalidAction() from None

        version = payload.pop('Version', None)
        if version != self.version:
            raise errors.InvalidAction(
                description=f"Unsupported API version {version}.")

        return action_name, payload

    def make_response(self, result):
        if isinstance(result, errors.AWSError):
            e = result
            xmlstr = xml.ERROR_TMPL.render(
                code=e.code,
                message=e.description,
                awscode=e.awscode,
                requestid=request.cornac_requestid,
                type='Sender' if e.code < 500 else 'Receiver',
            )
            status_code = e.code
        else:
            _, action = g.current_action.split(':', 1)
            xmlstr = xml.RESPONSE_TMPL.render(
                action=action,
                result=result,
                requestid=request.cornac_requestid,
                xmlns=self.xmlns,
            )
            status_code = 200

        response = make_response(xmlstr)
        response.status_code = status_code
        response.content_type = 'text/xml; charset=utf-8'
        response.headers['X-Amzn-RequestId'] = request.cornac_requestid
        return response


def resolve_payload_arrays(payload):
    """Groups all X.member.Y entries in a X list."""
    payload = payload.copy()
    keys = sorted(k for k in payload if '.member.' in k)
    for k in keys:
        value = payload.pop(k)
        argument, _, index = k.partition('.member.')
        index = int(index)
        array = payload.setdefault(argument, [])
        if index != (len(array) + 1):
            raise ValueError(f"Missing item before {index} in {argument}")
        array.append(value)
    return payload
