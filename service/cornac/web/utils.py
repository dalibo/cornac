from flask.json import jsonify as plain_jsonify


def jsonify(status=200, **kw):
    response = plain_jsonify(**kw)
    response.status_code = status
    return response
