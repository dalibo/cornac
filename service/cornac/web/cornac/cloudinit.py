import logging

from flask import make_response

from .blueprint import blueprint
from ..utils import jsonify
from ...core.cloudinit import build_metadata, build_userdata


logger = logging.getLogger(__name__)


# Implement NoCloud cloud-init metadata endpoint.
# See. https://cloudinit.readthedocs.io/en/18.5/topics/datasources/nocloud.html
@blueprint.route('/cloud-init/<hostname>/meta-data')
def cloudinit_metadata(hostname):
    return jsonify(**build_metadata(hostname))


# Implement NoCloud cloud-init metadata endpoint.
# See. https://cloudinit.readthedocs.io/en/18.5/topics/datasources/nocloud.html
@blueprint.route('/cloud-init/<hostname>/user-data')
def cloudinit_userdata(hostname):
    return make_response((
        build_userdata(hostname),
        {
            "Content-Type": "text/cloud-config",
        },
    ))
