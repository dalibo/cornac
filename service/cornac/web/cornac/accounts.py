import logging

from flask import request
from sqlalchemy.exc import IntegrityError

from .auth import login_required
from .blueprint import blueprint, get_account
from .. import errors
from ..auth import (
    check, check_access_to_rds,
)
from ..utils import jsonify
from ...core.model import db, Account, Identity
from ... import worker


logger = logging.getLogger(__name__)


@blueprint.route('/accounts', methods=['GET', 'POST'])
@login_required
def accounts():
    check()
    if 'GET' == request.method:
        accounts = [
            a.as_legacy_dict()
            for a in Account.query.all() if
            'ACTIVE' == a.data['Status']
            and check_access_to_rds(a)
        ]
        return jsonify(accounts=accounts)
    elif 'POST' == request.method:
        data = request.json or request.form
        if 'Alias' not in data:
            return jsonify(400, message='Missing Alias.')

        master = Account.query.get_master()
        try:
            account = Account.factory(
                name=data['Alias'],
                # Use master email because this legacy API does not ask one.
                email=master.data['Email'],
            )
            db.session.commit()
            worker.create_account.send(account.id)
        except IntegrityError:
            return jsonify(400, message="Account alias already used.")

        return jsonify(account=account.as_legacy_dict())


@blueprint.route('/accounts/<id_>', methods=['DELETE', 'GET', 'PATCH'])
@login_required
def account(id_):
    account = get_account(id_)

    check()

    if 'GET' == request.method:
        return jsonify(account=account.as_legacy_dict())
    elif 'DELETE' == request.method:
        if account.is_master:
            raise errors.InvalidParameterValue("Master account is protected.")

        if len(account.snapshots) > 0:
            raise errors.ValidationError("Account has ressources.")

        account.status = 'XDELETING'
        db.session.commit()
        worker.delete_account.send(account.id)
        return jsonify(200, message="Account deleted.")
    elif 'PATCH' == request.method:
        data = request.form or request.json
        try:
            account.name = data['NewAlias']
        except KeyError:
            raise errors.ValidationError("Missing NewAlias parameter.")
        group = (
            Identity.query.groups()
            .get_by(name=account.data['AdminGroupName'])
        )
        account.data['AdminGroupName'] = new_name = f'DBA-{account.name}'
        group.rename_to(new_name)
        db.session.commit()
        return jsonify(
            message="Account and associated group renamed.",
            account=account.as_legacy_dict(),
        )
