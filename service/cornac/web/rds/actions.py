# RDS-like service.
#
# Each method corresponds to a well-known RDS action, returning result as
# XML snippet.

import logging
from itertools import product
from uuid import uuid4

from flask import current_app, g
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from . import (
    checks,
    errors,
    xml,
)
from ...core import list_availability_zones
from ..auth import check as check_acl
from cornac import worker
from cornac.core.model import DBInstance, DBSnapshot, db
from cornac.utils import heredoc


logger = logging.getLogger(__name__)


@checks.CreateDBInstance
def CreateDBInstance(
        DBInstanceIdentifier, AvailabilityZone=None, **parameters):
    check_acl()

    if not parameters['MultiAZ'] and not AvailabilityZone:
        AvailabilityZone, *_ = list_availability_zones()

    instance = DBInstance.factory(DBInstanceIdentifier)
    instance.data.update(parameters)
    instance.data['AvailabilityZone'] = AvailabilityZone
    db.session.add(instance)

    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        logger.debug("Integrity error for new DBInstance:", exc_info=True)
        raise errors.DBInstanceAlreadyExists() from None

    worker.create_db_instance.send(instance.id)

    return xml.InstanceEncoder(instance).as_xml()


@checks.CreateDBSnapshot
def CreateDBSnapshot(*, DBInstanceIdentifier, DBSnapshotIdentifier, **__):
    instance = get_instance(DBInstanceIdentifier)
    check_status(instance, msg=heredoc(f"""\
    Cannot create a snapshot because the database instance
    {DBInstanceIdentifier} is not currently in the available state.
    """))

    snapshot = DBSnapshot.factory(instance, 'manual', DBSnapshotIdentifier)

    db.session.add(snapshot)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        raise errors.DBSnapshotAlreadyExists(identifier=snapshot.identifier) \
            from None

    worker.create_db_snapshot.send(snapshot.id)

    return xml.SnapshotEncoder(snapshot).as_xml()


@checks.DeleteDBInstance
def DeleteDBInstance(*, DBInstanceIdentifier, SkipFinalSnapshot, **command):
    if not current_app.has_snapshots:
        SkipFinalSnapshot = True

    instance = get_instance(DBInstanceIdentifier)

    if 'creating' == instance.status and not SkipFinalSnapshot:
        raise errors.InvalidDBInstanceState(
            f"Instance {DBInstanceIdentifier} is currently creating "
            "- a final snapshot cannot be taken.")

    if instance.data.get('DeletionProtection', False):
        raise errors.InvalidParameterCombination(
            "Cannot delete protected DB Instance, please disable deletion "
            "protection and try again.")

    snapshot_id = None
    if not SkipFinalSnapshot:
        snapshot_identifier = command['FinalDBSnapshotIdentifier']
        snapshot = DBSnapshot.factory(
            instance, 'manual', snapshot_identifier)
        db.session.add(snapshot)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            raise errors.DBSnapshotAlreadyExists(snapshot_identifier) from None
        snapshot_id = snapshot.id
    instance.status = 'deleting'
    db.session.commit()

    worker.delete_db_instance.send(
        instance.id, snapshot_id, command['DeleteAutomatedBackups'],
    )

    return xml.InstanceEncoder(instance).as_xml()


def DeleteDBSnapshot(*, DBSnapshotIdentifier, **command):
    snapshot = get_snapshot(DBSnapshotIdentifier)
    if snapshot.status not in ('available', 'failed'):
        raise errors.InvalidDBSnapshotState(
            "Cannot delete the snapshot because it is not currently in the "
            "available or failed state.",
        )

    snapshot.status = 'deleted'
    worker.delete_db_snapshot.send(snapshot.id)
    db.session.commit()
    return xml.SnapshotEncoder(snapshot).as_xml()


def DescribeDBInstances(**command):
    check_acl(resource=g.current_account.build_arn(resource='db:*'))
    qry = DBInstance.query.current()
    if 'DBInstanceIdentifier' in command:
        qry = qry.filter(
            DBInstance.identifier == command['DBInstanceIdentifier'])
    instances = qry.order_by(DBInstance.identifier).all()
    return xml.INSTANCE_LIST_TMPL.render(
        instances=[xml.InstanceEncoder(i) for i in instances])


def DescribeDBSnapshots(**command):
    check_acl(resource=g.current_account.build_arn(resource='snapshots:*'))
    qry = DBSnapshot.query.current()
    if 'DBSnapshotIdentifier' in command:
        qry = qry.filter(
            DBSnapshot.identifier == command['DBSnapshotIdentifier'])
    if 'DBInstanceIdentifier' in command:
        needle = dict(DBInstanceIdentifier=command['DBInstanceIdentifier'])
        qry = qry.filter(
            DBSnapshot.data.contains(needle)
        )
    snapshots = qry.order_by(DBSnapshot.identifier).all()
    return xml.SNAPSHOT_LIST_TMPL.render(
        snapshots=[xml.SnapshotEncoder(s) for s in snapshots])


def DescribeOrderableDBInstanceOptions(Engine, **command):
    check_acl()

    if Engine != 'postgres':
        raise errors.InvalidParameterValue("Invalid DB engine")

    default = dict(
        SupportsPerformanceInsights=current_app.has_temboard,
        AvailabilityZones=list_availability_zones(),
        # Same default as AWS RDS.
        DBInstanceClass='db.t2.micro',
        MultiAZCapable=len(list_availability_zones()) > 1,
    )
    versions = ['12', '11', '10', '9.6', '9.5']
    classes = current_app.config['INSTANCE_CLASSES']

    combinations = product(versions, classes)
    options_list = [
        dict(default, EngineVersion=v, DBInstanceClass=c)
        for v, c in combinations
    ]
    return xml.ORDERABLE_DB_INSTANCE_OPTIONS_LIST_TMPL.render(options_list=[
        xml.OrderableDBInstanceOptions(**options)
        for options in options_list
    ])


def RebootDBInstance(*, DBInstanceIdentifier, ForceFailover=False):
    allowed_statuses = (
        'available',
        'storage-optimization',
        'incompatible-credentials',
        'incompatible-parameters',
    )
    instance = get_instance(DBInstanceIdentifier, status=allowed_statuses)
    instance.status = 'rebooting'
    db.session.commit()
    worker.reboot_db_instance.send(instance.id, ForceFailover)
    return xml.InstanceEncoder(instance).as_xml()


@checks.RestoreDBInstanceFromDBSnapshot
def RestoreDBInstanceFromDBSnapshot(
        DBSnapshotIdentifier, DBInstanceIdentifier, **parameters):
    snapshot = get_snapshot(DBSnapshotIdentifier, status='available')
    instance = DBInstance.factory(DBInstanceIdentifier, extra=snapshot.data)
    instance.data.update(parameters)
    instance.recovery_token = str(uuid4())
    recovery_end_callback = instance.recovery_end_callback

    db.session.add(instance)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        raise errors.DBInstanceAlreadyExists() from None

    worker.restore_db_instance_from_db_snapshot.send(
        instance.id, snapshot.id,
        recovery_end_callback=recovery_end_callback,
    )

    return xml.InstanceEncoder(instance).as_xml()


@checks.RestoreDBInstanceToPointInTime
def RestoreDBInstanceToPointInTime(
        *, SourceDBInstanceIdentifier, TargetDBInstanceIdentifier,
        RestoreTime, UseLatestRestorableTime, **parameters):
    source = get_instance(SourceDBInstanceIdentifier)
    data = dict(source.data, **parameters)
    del data['DBInstanceIdentifier']
    del data['DeletionProtection']
    target = DBInstance.factory(TargetDBInstanceIdentifier, extra=data)
    target.recovery_token = str(uuid4())
    recovery_end_callback = target.recovery_end_callback
    source.data.pop('Endpoint', 'ignore-missing')

    if UseLatestRestorableTime:
        RestoreTime = None

    db.session.add(target)
    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        raise errors.DBInstanceAlreadyExists() from None

    worker.restore_db_instance_to_point_in_time.send(
        target.id, source.id, RestoreTime,
        recovery_end_callback=recovery_end_callback,
    )

    return xml.InstanceEncoder(target).as_xml()


def StartDBInstance(*, DBInstanceIdentifier):
    instance = get_instance(DBInstanceIdentifier, status='stopped')
    instance.status = 'starting'
    db.session.commit()
    worker.start_db_instance.send(instance.id)
    return xml.InstanceEncoder(instance).as_xml()


def StopDBInstance(DBInstanceIdentifier, DBSnapshotIdentifier=None):
    instance = get_instance(DBInstanceIdentifier, status='available')
    snapshot_id = None
    if DBSnapshotIdentifier:
        snapshot = DBSnapshot.factory(
            instance, 'manual', DBSnapshotIdentifier)
        db.session.add(snapshot)
        try:
            db.session.flush()
        except IntegrityError:
            db.session.rollback()
            raise errors.DBSnapshotAlreadyExists(DBSnapshotIdentifier) \
                from None
        snapshot_id = snapshot.id
    instance.status = 'stopping'
    db.session.commit()
    worker.stop_db_instance.send(instance.id, snapshot_id)
    return xml.InstanceEncoder(instance).as_xml()


def check_status(obj, status='available', msg=None):
    if status is None:
        return
    if isinstance(status, str):
        status = status,
    if obj.status not in status:
        cls = getattr(errors, f"Invalid{obj.__class__.__name__}State")
        if msg is None:
            statuses = ', '.join(status)
            msg = (
                f"{obj.__class__.__name__} must have state {statuses} but "
                f"actually has {obj.status}"
            )
        raise cls(msg)


def get_instance(identifier, status=None):
    try:
        instance = (
            DBInstance.query.current()
            .filter(DBInstance.identifier == identifier)
            .one())
    except NoResultFound:
        raise errors.DBInstanceNotFound(identifier)
    check_status(instance, status)
    check_acl(resource=instance)
    return instance


def get_snapshot(identifier, status=None):
    try:
        snapshot = (
            DBSnapshot.query.current()
            .filter(DBSnapshot.identifier == identifier)
            .one())
    except NoResultFound:
        raise errors.DBSnapshotNotFound(identifier)
    check_status(snapshot, status)
    check_acl(resource=snapshot)
    return snapshot
