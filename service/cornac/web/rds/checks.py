import logging
from functools import wraps

from ...core import list_availability_zones
from ...core.instanceclass import DBInstanceClass
from ...core.model import DBInstance
from ...operator import Operator
from ...utils import heredoc, validate_password_strength
from . import errors


logger = logging.getLogger(__name__)


#      T Y P E S

def AvailabilityZone(raw, param):
    zones = list_availability_zones()
    if raw not in zones:
        raise errors.InvalidParameterValue(
            f"{raw} is not a valid availability zone")
    return raw


def Boolean(raw, param):
    if raw not in ('false', 'true'):
        raise ValueError("Invalid boolean value")
    return 'true' == raw


def Engine(raw, param):
    if 'postgres' != raw:
        raise errors.InvalidParameterValue('Invalid DB Engine')
    return raw


def Identifier(raw, param):
    try:
        return DBInstance.check_identifier(raw)
    except ValueError:
        raise errors.InvalidParameterValue(heredoc(f"""\
        The parameter {param} is not a valid identifier.
        Identifiers must begin with a letter; must contain only ASCII letters,
        digits, and hyphens; and must not end with a hyphen or contain two
        consecutive hyphens.
        """))


def Integer(raw, param):
    return int(raw)


def InstanceClass(raw, param):
    try:
        DBInstanceClass.parse(raw)
        return raw
    except ValueError as e:
        logger.debug("Invalid %s %s: %s.", param, raw, e)
        raise errors.InvalidParameterValue(
            f"Invalid DB Instance class: {raw}")


def Password(raw, param):
    try:
        validate_password_strength(raw)
        return raw
    except ValueError as e:
        raise errors.InvalidParameterValue(f"Bad master user password: {e}")


def PostgresRolename(raw, param):
    if raw in Operator.RESERVED_ROLES:
        raise errors.InvalidParameterValue(heredoc(f"""\
        {param} {raw} cannot be used as it
        is a reserved word used by the engine"""))

    return raw


def PostgresMajorVersion(raw, param):
    # Naïve version validation.

    try:
        value = int(raw)
        if value > 10:
            return raw
    except ValueError:
        try:
            value = float(raw)
            if value < 10:
                return raw
        except ValueError:
            pass
    raise errors.InvalidParameterCombination(
        f"Cannot find version {raw} for postgres")


def Raw(raw, param):
    return raw


#       S C H E M A S


REQUIRED = MISSING = object()
UNDEFINED = object()


class EndpointSchema:
    def __init__(self, parameters=None):
        # Simple alias map. Applied first.
        self.aliases = {}
        # Default **processed** value.
        self.defaults = {}
        # Map parameter name to a callable processing it.
        self.types = {}

        for parameter in parameters or []:
            self.register(parameter)

    def __call__(self, func):
        # Decorate an AWS API handler to validate it's inputs.

        @wraps(func)
        def wrapper(**parameters):
            self.rename_parameters(parameters)
            parameters = self.validate_values(parameters)
            parameters = dict(self.defaults, **parameters)
            self.validate_required(parameters)
            self.validate_combinations(**parameters)
            return func(**parameters)

        return wrapper

    def register(self, parameter):
        if parameter.name in self.types:
            raise Exception(f"Parameter {parameter.name} defined twice.")

        self.types[parameter.name] = parameter.type_
        if parameter.default is not UNDEFINED:
            self.defaults[parameter.name] = parameter.default
        for alias in parameter.aliases:
            self.aliases[alias] = parameter.name

    def rename_parameters(self, parameters):
        for alias, name in self.aliases.items():
            try:
                parameters[name] = parameters.pop(alias)
            except KeyError:
                pass

    def validate_required(self, parameters):
        for name, default in self.defaults.items():
            if default is not REQUIRED:
                continue

            value = parameters.get(name, MISSING) or MISSING

            if value is MISSING:
                raise errors.InvalidParameterValue(heredoc(f"""\
                The parameter {name} must be provided and must not be blank.
                """))

    def validate_values(self, raw):
        parameters = {}

        for k, v in raw.items():
            try:
                validator = self.types[k]
            except KeyError:
                logger.debug("Ignoring parameter %s.", k)
                continue

            try:
                parameters[k] = validator(v, k)
            except ValueError:
                raise errors.InvalidParameterValue(
                    f"Parameter {k} is not valid.")

        return parameters

    def validate_combinations(self, **parameters):
        if 'MultiAZ' in parameters:
            self.validate_availibility_zone(**parameters)

    def validate_availibility_zone(self, MultiAZ, **parameters):
        if MultiAZ and 'AvailabilityZone' in parameters:
            raise errors.InvalidParameterCombination(
                "Requesting a specific availability zone is not valid"
                " for Multi-AZ instances."
            )


class Parameter:
    def __init__(
            self, name, type_=Raw, default=UNDEFINED, aliases=None,
            required=False):
        self.name = name
        self.type_ = type_
        if required:
            default = REQUIRED
        self.default = default
        self.aliases = aliases or []

    def bake(self, **kw):
        kw.setdefault('name', self.name)
        kw.setdefault('type_', self.type_)
        kw.setdefault('default', self.default)
        kw.setdefault('aliases', self.aliases)
        return self.__class__(**kw)


class KnownParameters:
    AvailabilityZone = Parameter(
        'AvailabilityZone', type_=AvailabilityZone,
    )
    DBInstanceClass = Parameter('DBInstanceClass', type_=InstanceClass)
    DBInstanceIdentifier = Parameter(
        'DBInstanceIdentifier', type_=Identifier,
        required=True,
    )
    DBSnapshotIdentifier = Parameter(
        'DBSnapshotIdentifier', type_=Identifier,
        required=True,
    )
    MultiAZ = Parameter(
        'MultiAZ', type_=Boolean,
    )


# Instanciate decorator factory.
CreateDBInstance = EndpointSchema([
    Parameter(
        'AllocatedStorage', type_=Integer,
        required=True,
    ),
    KnownParameters.AvailabilityZone,
    KnownParameters.DBInstanceClass.bake(required=True),
    KnownParameters.DBInstanceIdentifier,
    Parameter('Engine', type_=Engine, required=True),
    Parameter(
        'EngineVersion', type_=PostgresMajorVersion,
        default='12',
    ),
    Parameter('MasterUsername', type_=PostgresRolename, required=True),
    Parameter('MasterUserPassword', type_=Password, required=True),
    KnownParameters.MultiAZ.bake(default=False),
    Parameter(
        'PerformanceInsightsEnabled', type_=Boolean, default=False,
        aliases=['EnablePerformanceInsights'],
    ),
])

CreateDBSnapshot = EndpointSchema([
    KnownParameters.DBInstanceIdentifier,
    KnownParameters.DBSnapshotIdentifier,
])


class DeleteDBInstanceSchema(EndpointSchema):
    def validate_combinations(
            self, SkipFinalSnapshot, **parameters):
        if not SkipFinalSnapshot and \
           'FinalDBSnapshotIdentifier' not in parameters:
            raise errors.InvalidParameterCombination(heredoc("""\
            FinalDBSnapshotIdentifier is required unless SkipFinalSnapshot
            is specified.
            """))


DeleteDBInstance = DeleteDBInstanceSchema([
    KnownParameters.DBInstanceIdentifier,
    Parameter('DeleteAutomatedBackups', type_=Boolean, default=True),
    Parameter('FinalDBSnapshotIdentifier', type_=Identifier),
    Parameter('SkipFinalSnapshot', type_=Boolean, default=False),
])


RestoreDBInstanceFromDBSnapshot = EndpointSchema([
    KnownParameters.AvailabilityZone,
    KnownParameters.DBInstanceClass,
    KnownParameters.DBInstanceIdentifier,
    KnownParameters.DBSnapshotIdentifier,
    Parameter('Engine', type_=Engine, default='postgres'),
    KnownParameters.MultiAZ,
])


class RestoreDBInstanceToPointInTimeSchema(EndpointSchema):
    def validate_combinations(self, RestoreTime, UseLatestRestorableTime, **_):
        if not UseLatestRestorableTime and not RestoreTime:
            raise errors.InvalidParameterCombination(heredoc("""\
            If UseLatestRestorableTime is not true,
            the RestoreTime parameter must be specified.
            """))


RestoreDBInstanceToPointInTime = RestoreDBInstanceToPointInTimeSchema([
    KnownParameters.AvailabilityZone,
    KnownParameters.DBInstanceClass,
    KnownParameters.MultiAZ,
    Parameter('SourceDBInstanceIdentifier', type_=Identifier, required=True),
    Parameter('TargetDBInstanceIdentifier', type_=Identifier, required=True),
    Parameter('UseLatestRestorableTime', type_=Boolean, default=False),
    Parameter('RestoreTime', type_=Raw, default=None),
])
