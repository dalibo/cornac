# Cornac Webservice Virtual Appliance

This sub-project provides scripts to build, export and deploy Cornac Webservice
as a standalone virtual appliance.

The build requires Docker, docker-compose, 8GB of disk space available and 4GB
of RAM.

The `Makefile` is the main entrypoint for managing the project. `make`
builds `cornac.ova` ready for deployment on vCenter. Here are available make
commands:

- `build` -- Build OVA in a container.
- `buidenv` -- Enter an interactive build container.
- `clean` -- Clean artefacts.
- `distclean` -- Clear cache, containers.
- `test` -- Deploy ova in libvirt.

The average time for building a virtual appliance with warm cache is 12 minutes.


## Viewing console

`make buildenv` presents an interactive shell with all tools for building ova.
To view the VM console, you need to install `qemu-system-gui` package inside
the container. If this package is missing, packer fails with `qemu.centos7:
Error launching VM: Qemu failed to start. Please run with PACKER_LOG=1 to get
more info.`.

``` console
$ make buildenv
docker-compose up -d
...
root@gilwell:/workspace/appliance# apt update -y && apt install -y --no-install-recommends qemu-system-gui
Get:1 http://deb.debian.org/debian bullseye InRelease [146 kB]
...
Setting up qemu-system-gui:amd64 (1:5.2+dfsg-9) ...
Processing triggers for libc-bin (2.31-11) ...
root@gilwell:/workspace/appliance# make clean ova fixperms
rm -frv cornac-sda.vmdk cornac.ova build/*
packer build \
...
==> qemu.centos7: Connecting to VM via VNC (127.0.0.1:5971)
...
```

Interactive build container is configured with `http_proxy` pointing to
`cornac-proxy-cache.docker`. This domain name relies on dnsdock and `.docker`
resolution on your host.


## Testing with libvirt

To test cornac.ova, you need virt-v2v and virtinst on the host. Execute `make
test` to deploy the .ova in your default libvirt URI. Before running the VM,
check network settings. Once appliance is booted, SSH into it as root with the
password defined by `ssh_password` setting in `scripts/cornac.pkr.hcl`.


## Deploying to vCenter

Once cornac.ova is ready, deploy it using vSphere Web Client. You can also use
`deploy_ova.py` script, adapted from `pyvmomi` community samples. This script
requires a cornac webservice development environment. Run it as following:

``` console
$ ./deploy_ova.py --disable_ssl_verification --host vsphere.lan.company.tld --user me@vsphere.local
```

See `./deploy_ova.py --help` for further details.

Once appliance is up and running, access it through SSH as root with default
password `C0rn@c`. The default hostname is `cornac`.
