packer {
  required_version = ">= 1.6, < 2.0.0"
}

variable "http_proxy" {
  type = string
  default = env("http_proxy")
}

variable "headless" {
  type = bool
  default = true
}

locals {
  # Search files is relative to project root.
  custom_files = fileset("../custom/", "*")
}

build {
  source "qemu.centos7" {
    vm_name = "cornac"
    output_directory = "/tmp/packer-output/packer"
    format = "qcow2"
    # We'll use virt-sparsify later. Much more efficient.
    skip_compaction = true
    cpus = 2
    memory = 2048
    disk_size = "8G"
    disk_interface = "virtio-scsi"
    headless = var.headless

    http_directory = "scripts/"

    iso_url = "http://centos-mirror.usessionbuddy.com/7.9.2009/isos/x86_64/CentOS-7-x86_64-NetInstall-2009.iso"
    # From http://centos-mirror.usessionbuddy.com/7.9.2009/isos/x86_64/sha256sum.txt
    iso_checksum = "sha256:b79079ad71cc3c5ceb3561fff348a1b67ee37f71f4cddfec09480d4589c191d6"

    boot_wait = "5s"
    boot_command = [
      "<tab>",
      " http_proxy=${ var.http_proxy }",
      " inst.cmdline",
      " inst.headless",
      " ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg",
      "<enter>",
      "<wait9m>",
    ]

    ssh_port = 22
    ssh_username = "root"
    ssh_password = "C0rn@c"
    ssh_timeout = "30m"
  }

  dynamic "provisioner" {
    for_each = local.custom_files
    iterator = file
    labels = ["file"]

    content {
      # File provisionner source is relative to packer working directory.
      source = "custom/${ file.value }"
      destination = "/root/custom/${ file.value }"
    }
  }

  provisioner "shell" {
    script = "scripts/postinstall.sh"
    environment_vars = [
      "http_proxy=${ var.http_proxy }",
    ]
  }
}

source "qemu" "centos7" {
}
