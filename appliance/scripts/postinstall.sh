#!/bin/bash -eux

# Streamline initramfs and ensure VMWare drivers are embedded.
cat >/etc/dracut.conf.d/cornac.conf <<EOF
omit_dracutmodules+="mdraid"
# Ensure VMWare drivers are included.
add_drivers+="vmxnet3 vmw_balloon vmwgfx vmw_pvscsi vmw_vmci vmw_vsock_vmci_transport"
EOF
dracut --force --verbose

# Setup YUM and extra repositories
sed -i /^keepcache=/s/=0/=1/ /etc/yum.conf
yum install -y epel-release https://yum.dalibo.org/labs/dalibo-labs-2-1.noarch.rpm
yum makecache fast

# Install cornac stack
yum install -y \
    nginx \
    vim \
    /root/custom/*.rpm \
    virt-install \
    "${NULL-}"

# Hook custom scripts.
for script in /root/custom/*.sh ; do
	if [ -r "$script" ] ; then
		"$script"
	fi
done

# Make some room.
rm -rf /root/custom
(
    find /usr/share/locale/* | grep -Ev 'en_US|fr';
    find /usr/share/man/* | grep -v '/man/man';
) | xargs rm -rf

/opt/cornac/bin/cornac --version
/opt/cornac/bin/cornac routes

passwd --expire root

# Ensure disk is synchronize. Trying to workaround some race condition where
# packer qcow2 image has empty files.
sync
