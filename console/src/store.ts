import Vue from 'vue';
import Vuex from 'vuex';
import { config, Credentials, RDS } from 'aws-sdk';
import router from './router';
import moment from 'moment';
import { awsOnly, cornacEndpoint, rdsConfig } from '@/App.vue';
import { IAccount } from '@/interfaces';
import { STS } from 'aws-sdk';
import axios from 'axios';
import * as _ from 'lodash';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    accounts: [],
    currentAccount: {AccountId: null, AccountAlias: '...'},
    accessKeyId: '',
    secretAccessKey: '',
    sessionToken: '',
    rdsConfig: {
      accessKeyId: '',
      secretAccessKey: '',
      sessionToken: '',
    },
    username: '',
    policies: [],
    expiration: null,
    instances: null,
    backups: null,
    loadingAccounts: false,
    loadingInstances: false,
    loadingBackups: false,
  },
  mutations: {
    accounts(state, accounts) {
      state.accounts = accounts;
    },
    username(state, username) {
      state.username = username;
    },
    login(state, data) {
      const key = data.credentials.AccessKeyId;
      const secret = data.credentials.SecretAccessKey;
      const token = data.credentials.SessionToken;
      state.accessKeyId = key;
      state.secretAccessKey = secret;
      state.sessionToken = token;
      state.expiration = data.credentials.Expiration;
      state.rdsConfig.accessKeyId = key;
      state.rdsConfig.secretAccessKey = secret;
      state.rdsConfig.sessionToken = token;

      if (data.policies) {
        state.policies = data.policies;
      }

      const credentials = new Credentials(key, secret, token);
      config.update({credentials});
    },
    logout(state) {
      state.username = '';
      state.accounts = [];
      state.currentAccount = {AccountId: null, AccountAlias: '...'};
      state.accessKeyId = '';
      state.secretAccessKey = '';
      state.sessionToken = '';
      state.expiration = null;
      state.rdsConfig.accessKeyId = '';
      state.rdsConfig.secretAccessKey = '';
      state.rdsConfig.sessionToken = '';
      state.instances = null;
      state.backups = null;
      config.update({credentials: new Credentials('', '')});
    },
    switchAccount(state, data) {
      let credentials: Credentials;
      if (data.Credentials == null) {
        credentials = new Credentials(
          config.credentials!.accessKeyId,
          config.credentials!.secretAccessKey,
          config.credentials!.sessionToken,
        );
      } else {
        credentials = new Credentials(
          data.Credentials.AccessKeyId,
          data.Credentials.SecretAccessKey,
          data.Credentials.SessionToken,
        );
      }
      state.rdsConfig.accessKeyId = credentials.accessKeyId;
      state.rdsConfig.secretAccessKey = credentials.secretAccessKey;
      state.rdsConfig.sessionToken = credentials.sessionToken;
      state.currentAccount = data.account;
    },
    setInstances(state, instances) {
      state.instances = instances;
    },
    setBackups(state, backups) {
      state.backups = backups;
    },
    SET_LOADING_ACCOUNTS_STATUS(state, status) {
      state.loadingAccounts = status;
    },
    SET_LOADING_INSTANCES_STATUS(state, status) {
      state.loadingInstances = status;
    },
    SET_LOADING_BACKUPS_STATUS(state, status) {
      state.loadingBackups = status;
    },
  },
  actions: {
    renewToken() {
      axios.post(cornacEndpoint + '/login/renew', {
        session_token: this.state.sessionToken,
      })
        .then((response) => {
          this.commit('login', response.data);
        })
        .catch((error) => {
          Vue.notify({
            title: 'Error',
            text: 'Error while automatically renewing the token',
            type: 'error',
          });
          this.commit('logout');
          router.push('/login');
        });
    },
    initializeStore(context) {
      Object.assign(context.state, JSON.parse(localStorage.getItem('store') || '{}'));
      this.replaceState(context.state);

      // Configure credentials from localStorage
      config.update({
        credentials: new Credentials(
          context.state.accessKeyId,
          context.state.secretAccessKey,
          context.state.sessionToken),
      });
    },
    fetchAccounts(context) {
      context.commit('SET_LOADING_ACCOUNTS_STATUS', true);
      axios.get(cornacEndpoint + '/accounts', {
        headers: context.getters.cornacHeaders,
      })
        .then((response) => {
          this.commit('accounts', response.data.accounts);
          if (response.data.accounts.length) {
            // take account from local storage if available
            const account = _.find(response.data.accounts, (a) => {
              return a.AccountId ===
                context.state.currentAccount.AccountId;
            });
            this.dispatch('switchAccount', account || response.data.accounts[0]);
          }
          context.commit('SET_LOADING_ACCOUNTS_STATUS', false);
        });
    },
    switchAccount(context, account: IAccount) {
      const RoleArn = account.AdminRoleArn;
      if (RoleArn) {
        const sts = new STS();
        sts.assumeRole(
          {RoleArn, RoleSessionName: 'Console'},
          (err, data: any) => {
            if (!err) {
              this.commit('switchAccount', {account, Credentials: data.Credentials});
            } else {
              Vue.notify({
                title: 'Error',
                text: 'Error while switching account: ' + err,
                type: 'error',
              });
            }
          },
        );
      } else {
        this.commit('switchAccount', {account, Credentials: null});
      }
    },
    fetchInstances(context) {
      context.commit('SET_LOADING_INSTANCES_STATUS', true);
      const rds: RDS = context.getters.rds;
      rds.describeDBInstances({}, (err, data: any) => {
        if (err) {
          context.commit('setInstances', []);
          if (err.statusCode === 403) {
            context.commit('logout');
            router.push('/login');
          }
          Vue.notify({
            title: 'Error',
            text: 'There was an error listing your instances: ' + err.message,
            type: 'error',
          });
        } else {
          context.commit('setInstances', data.DBInstances);
        }
        context.commit('SET_LOADING_INSTANCES_STATUS', false);
      });
    },
    fetchBackups(context) {
      context.commit('SET_LOADING_BACKUPS_STATUS', true);
      const rds: RDS = context.getters.rds;
      rds.describeDBSnapshots({}, (err, data: any) => {
        if (err) {
          if (err.statusCode === 403) {
            context.commit('logout');
            router.push('/login');
          }
          Vue.notify({
            title: 'Error',
            text: 'There was an error listing your backups: ' + err.message,
            type: 'error',
          });
          return;
        } else {
          context.commit('setBackups', data.DBSnapshots);
        }
        context.commit('SET_LOADING_BACKUPS_STATUS', false);
      });
    },
  },
  getters: {
    cornacHeaders: (state) => ({'X-Amz-Security-Token': state.sessionToken}),
    isLoggedIn: (state) => !!state.username && !!state.accessKeyId && !!state.secretAccessKey,
    isAdmin: (state) => {
      const policies = state.policies as string[];
      return policies.indexOf('IAMFullAccess') !== -1;
    },
    sessionToken: (state) => state.sessionToken,
    rds: (state) => {
      const credentials = new Credentials(
        state.rdsConfig.accessKeyId,
        state.rdsConfig.secretAccessKey,
        state.rdsConfig.sessionToken,
      );
      rdsConfig.update({credentials});
      return new RDS(rdsConfig);
    },
  },
});

// Subscribe to store updates
store.subscribe((mutation, state) => {
  const toSave: any = {};
  Object.assign(toSave, state);
  // Don't save all state properties, exclude some
  delete toSave.instances;
  delete toSave.backups;
  // Store the state object as a JSON string
  localStorage.setItem('store', JSON.stringify(toSave));
});
export default store;

let expirationTimeout: number;
// How many seconds before expiration should we ask for a renew of the token
const delayBeforeExpiration: number = 5 * 60;

// Watch for any change on expiration and set a timeout accordingly
store.watch(
  (state, getters) => state.expiration,
  (newExpiration, oldValue) => {
    window.clearTimeout(expirationTimeout);
    if (!newExpiration) {
      return;
    }
    const duration = moment(newExpiration!).diff(moment()) - delayBeforeExpiration * 1000;
    expirationTimeout = window.setTimeout(() => {
      store.dispatch('renewToken');
    }, duration);
  },
);
