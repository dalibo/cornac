import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import AdministrationMenu from '@/views/AdministrationMenu.vue';
import AdministrationHeading from '@/views/AdministrationHeading.vue';
import Databases from '@/views/Databases.vue';
import DatabasesMenu from '@/views/DatabasesMenu.vue';
import DatabasesHeading from '@/views/DatabasesHeading.vue';
import InstancesHome from '@/views/InstancesHome.vue';
import Instance from '@/views/Instance.vue';
import InstanceNew from '@/views/InstanceNew.vue';
import Backups from '@/views/Backups.vue';
import BackupsHome from '@/views/BackupsHome.vue';
import Backup from '@/views/Backup.vue';
import AccountsHome from '@/views/AccountsHome.vue';
import AccountNew from '@/views/AccountNew.vue';
import Account from '@/views/Account.vue';
import UsersHome from '@/views/UsersHome.vue';
import UserNew from '@/views/UserNew.vue';
import User from '@/views/User.vue';
import UserResetPassword from '@/views/UserResetPassword.vue';
import VirtualIpsHome from '@/views/VirtualIpsHome.vue';
import store from '@/store';
import { awsOnly } from '@/App.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/instances',
    },
    {
      path: '/instances',
      meta: {
        service: 'Databases',
      },
      components: {
        default: Databases,
        menu: DatabasesMenu,
        heading: DatabasesHeading,
      },
      children: [{
        path: '',
        name: 'instances',
        component: InstancesHome,
      }, {
        path: 'new',
        name: 'instance_new',
        component: InstanceNew,
      }, {
        path: ':id',
        name: 'instance',
        component: Instance,
      }],
    },
    {
      path: '/backups',
      meta: {
        service: 'Databases',
      },
      components: {
        default: Databases,
        menu: DatabasesMenu,
        heading: DatabasesHeading,
      },
      children: [{
        path: '',
        name: 'backups',
        component: BackupsHome,
      },
      {
        path: ':id',
        name: 'backup',
        component: Backup,
      }],
    },
    {
      path: '/users',
      meta: {
        service: 'Administration',
      },
      components: {
        default: Home,
        menu: AdministrationMenu,
        heading: AdministrationHeading,
      },
      children: [{
        path: '',
        name: 'users',
        component: UsersHome,
      }, {
        path: ':username',
        name: 'user',
        component: User,
      }, {
        path: 'new',
        name: 'user_new',
        component: UserNew,
      }],
    },
    {
      path: '/accounts',
      meta: {
        service: 'Administration',
      },
      components: {
        default: Home,
        menu: AdministrationMenu,
        heading: AdministrationHeading,
      },
      children: [{
        path: '',
        name: 'accounts',
        component: AccountsHome,
      }, {
        path: ':id',
        name: 'account',
        component: Account,
      }, {
        path: 'new',
        name: 'account_new',
        component: AccountNew,
      }],
    },
    {
      path: '/virtualips',
      meta: {
        service: 'Administration',
      },
      components: {
        default: Home,
        menu: AdministrationMenu,
        heading: AdministrationHeading,
      },
      children: [{
        path: '',
        name: 'virtualips',
        component: VirtualIpsHome,
      }],
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('./views/Login.vue'),
      meta: {
        hideNavigation: true,
      },
    },
    {
      path: '/loginaccesskey',
      name: 'loginaccesskey',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('./views/LoginAccessKey.vue'),
      meta: {
        hideNavigation: true,
      },
    },
    {
      path: '/profile',
      name: 'profile',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('./views/Profile.vue'),
    },
    {
      path: '/reset_password/',
      name: 'query_reset_password',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('./views/UserResetPasswordQuery.vue'),
      meta: {
        hideNavigation: true,
      },
    },
   {
      path: '/reset_password/:token',
      name: 'reset_password',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('./views/UserResetPassword.vue'),
      meta: {
        hideNavigation: true,
      },
    },
  ],
});

// Redirect user to login page if not authenticated
router.beforeEach((to, from, next) => {
  // Workaround to make sure this happens after store is initialized
  window.setTimeout(() => {
    if (to.name !== 'login' && to.name !== 'loginaccesskey' &&
        to.name !== 'reset_password' && to.name !== 'query_reset_password' &&
        !store.getters.isLoggedIn) {
      if (awsOnly) {
        next('/loginaccesskey');
      } else {
        next('/login');
      }
    } else if (to.name === 'login' && awsOnly) {
      // Redirect to Access key login in case of AWS
      next('/loginaccesskey');
    } else {
      next();
    }
  }, 0);
});
export default router;
