import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { config, Credentials } from 'aws-sdk';
import VueTimeago from 'vue-timeago';
import LoadingEllipsis from '@/components/LoadingEllipsis.vue';

Vue.use(VueTimeago, {
  locale: 'en',
});

Vue.config.productionTip = false;

const region = 'eu-west-2';
const accessKeyId = localStorage.getItem('AWSAccessKeyId') || '';
const secretAccessKey = localStorage.getItem('AWSSecretAccessKey') || '';
config.apiVersions = {
  rds: '2014-10-31',
};
config.update({
  region,
  credentials: new Credentials(accessKeyId, secretAccessKey),
});

new Vue({
  router,
  store,
  render: (h) => h(App),
  mounted() {
    this.$store.dispatch('initializeStore');
  },
}).$mount('#app');

Vue.component('loading-ellipsis', LoadingEllipsis);
