import * as _ from 'lodash';

export function padStart(value: string, length: number, chars: string): string {
  return _.padStart(value, length, chars);
}

export function formatGigabytes(value: number) {
  const k = 1024;
  const units = ['GB', 'TB'];
  const i = Math.floor(Math.log(value) / Math.log(k));
  const compiled = _.template('${value} ${unit}');
  const valueString = (value / Math.pow(k, i)).toLocaleString();
  return compiled({value: valueString, unit: units[i]});
}
