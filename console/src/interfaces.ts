interface Endpoint {
  Address: string;
  Port: number | null;
}

export interface IAccount {
  AccountId: string;
  AccountAlias: string;
  AdminGroupName: string;
  AdminRoleArn: string;
}

export interface IAccessKey {
  AccessKeyId: string;
  SecretAccessKey: string;
}

export interface IGroup {
  GroupName: string;
}

export interface IInstance {
  DBInstanceIdentifier: string;
  DBInstanceStatus: string;
  Engine: string;
  EngineVersion: number | null;
  Endpoint: Endpoint;
  MasterUsername: string;
  AllocatedStorage: number | null;
  InstanceCreateTime: string;
  AvailabilityZone: string | null;
  MultiAZ: boolean | null;
  LatestRestorableTime: string;
  DeletionProtection: boolean;
  PerformanceInsightsEnabled: boolean;
}

export interface IUser {
  UserName: string;
  UserId: string;
}

export interface IIPRange {
  allocated: number;
  comment: string;
  id: number;
  range: string;
  size: number;
}
