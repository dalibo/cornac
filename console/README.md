# Cornac Console

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Compiler le paquet RPM

Pour générer le paquet RPM, exécuter le service compose `rpm`.

``` console
$ docker-compose run --rm rpm
...
+ chown -R 1000:1000 /workspace/dist /workspace/node_modules
$
```

Le paquet rpm `cornac-console` se trouve dans le dossier dist/.

### Update of NPM dependencies 

The npm packages update may be required. For this, we rely on snyk.

Install `snyk` as explained in [https://snyk.io/test/].

```
snyk test
```

Use `SNYK_TOKEN=XXXX snyk test` if `snyk auth` fails.

Then follow instuctions.
