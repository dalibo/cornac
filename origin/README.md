# Provisionning origin VM

This directory is an Ansible project to configure a CentOS 7 or 8 virtual
machine as a PostgreSQL machine template for cornac service.

``` console
$ ansible-playbook -i cornac--origin.virt, origin.yml
```
