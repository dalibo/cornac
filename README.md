# cornac - Enterprise-class Postgres deployment

cornac implements a RDS compatible API on top of a custom engine called
*Operator*.

The project is subdivided as following:

- `service/` is a Python project implementing RDS-compatible webservice.
- `console/` is a VueJS project implementing an RDS web interface.
- `origin/` is an Ansible project to maintain Postgres virtual machine template.
- `appliance/` is a libvirt/kickstart project to build Virtual appliance as OVA.


Here is a screenshot of the console running on top of the webservice:

![Screenshot](./screenshot.png)

Documentation is at [docs/](./docs).
